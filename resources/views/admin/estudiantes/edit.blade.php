@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Editar Estudiantes</h1>
@stop

@section('content')
@if (session('info'))

<div class="alert alert-success" >
    <strong>{{session('info')}}</strong>
</div>
    
@endif
<div class="card">
    <div class="card-body">
        {!! Form::Model($estudiante,['route'=>['admin.estudiantes.update',$estudiante], 'autocomplete'=>'off','method'=>'put']) !!}

        <div class="form-group">
            {!! Form::label('nombre', 'Nombre:') !!}
            {!! Form::text('nombre', null, ['class'=>'form-control','placeholder'=>'nombre']) !!}
            @error('nombre')
                <span class="text-danger" > {{$message}}</span>
            @enderror
        
        </div>
        <div class="form-group">
            {!! Form::label('apellido', 'Apellido:') !!}
            {!! Form::text('apellido', null, ['class'=>'form-control','placeholder'=>'nombre']) !!}
            @error('apellido')
                <span class="text-danger" > {{$message}}</span>
            @enderror
        </div>

        <div class="form-group">
            {!! Form::label('cuenta', 'Número de Cuenta:') !!}
            {!! Form::text('cuenta', null, ['class'=>'form-control','placeholder'=>'nombre']) !!}
            @error('cuenta')
                <span class="text-danger" > {{$message}}</span>
            @enderror
        
        </div>

        <div class="form-group">
            {!! Form::label('telefono', 'Telefono:') !!}
            {!! Form::text('telefono', null, ['class'=>'form-control','placeholder'=>'nombre']) !!}
            @error('telefono')
                <span class="text-danger" > {{$message}}</span>
            @enderror
        </div>

        <div class="form-group">
            {!! Form::label('correo', 'Correo:') !!}
            {!! Form::text('correo', null, ['class'=>'form-control','placeholder'=>'nombre']) !!}
            @error('correo')
                <span class="text-danger" > {{$message}}</span>
            @enderror
        
        </div>



        <div class="form-group">
            {!! Form::label('carrera_id', 'Carrera') !!}
            {!! Form::select('carrera_id',$carreras,null,['class'=>'form-control','placeholder' => 'Seleccione...'])!!}
            @error('carrera_id')
                <span class="text-danger" > {{$message}}</span>
            @enderror
        
        </div>
       
        {!! Form::submit("Actualizar", ['class'=>'btn btn-primary']) !!}


        {!! Form::close() !!}
    </div>
</div>
@stop
