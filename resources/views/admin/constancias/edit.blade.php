@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Información de  Estudiantes</h1>
@stop

@section('content')
@if (session('info'))

<div class="alert alert-success" >
    <strong>{{session('info')}}</strong>
</div>
    
@endif
<div class="container">
    <div class="main-body">
    
          
          <!-- /Breadcrumb -->
          <div class="row gutters-sm">
            <div class="col-md-4 mb-3">
              <div class="card">
                <div class="card-body">
                  <div class="d-flex flex-column align-items-center text-center">
                    <img src="{{Storage::url($estudiante->foto)}}" alt="Admin" class="rounded-circle" width="150">
                    <div class="mt-3">
                      <h4>{{$estudiante->nombre.' '.$estudiante->apellido}}</h4>
                      <p class="text-secondary mb-1">{{$estudiante->cuenta}}</p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card mt-3">
                <ul class="list-group list-group-flush">
                  <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                    <h6 class="mb-0"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-phone" viewBox="0 0 16 16">
                        <path d="M11 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h6zM5 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H5z"/>
                        <path d="M8 14a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"/>
                      </svg></i>  Teléfono:</h6>
                    <span class="text-secondary"> {{$estudiante->telefono}}</span>
                  </li>
                  <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                    <h6 class="mb-0"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-envelope" viewBox="0 0 16 16">
                        <path d="M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4zm2-1a1 1 0 0 0-1 1v.217l7 4.2 7-4.2V4a1 1 0 0 0-1-1H2zm13 2.383-4.758 2.855L15 11.114v-5.73zm-.034 6.878L9.271 8.82 8 9.583 6.728 8.82l-5.694 3.44A1 1 0 0 0 2 13h12a1 1 0 0 0 .966-.739zM1 11.114l4.758-2.876L1 5.383v5.73z"/>
                      </svg>  Correo:</h6>
                    <span class="text-secondary"> {{$estudiante->correo}}</span>
                  </li>
                  <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                    <h6 class="mb-0"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-building" viewBox="0 0 16 16">
                        <path fill-rule="evenodd" d="M14.763.075A.5.5 0 0 1 15 .5v15a.5.5 0 0 1-.5.5h-3a.5.5 0 0 1-.5-.5V14h-1v1.5a.5.5 0 0 1-.5.5h-9a.5.5 0 0 1-.5-.5V10a.5.5 0 0 1 .342-.474L6 7.64V4.5a.5.5 0 0 1 .276-.447l8-4a.5.5 0 0 1 .487.022zM6 8.694 1 10.36V15h5V8.694zM7 15h2v-1.5a.5.5 0 0 1 .5-.5h2a.5.5 0 0 1 .5.5V15h2V1.309l-7 3.5V15z"/>
                        <path d="M2 11h1v1H2v-1zm2 0h1v1H4v-1zm-2 2h1v1H2v-1zm2 0h1v1H4v-1zm4-4h1v1H8V9zm2 0h1v1h-1V9zm-2 2h1v1H8v-1zm2 0h1v1h-1v-1zm2-2h1v1h-1V9zm0 2h1v1h-1v-1zM8 7h1v1H8V7zm2 0h1v1h-1V7zm2 0h1v1h-1V7zM8 5h1v1H8V5zm2 0h1v1h-1V5zm2 0h1v1h-1V5zm0-2h1v1h-1V3z"/>
                      </svg>  Centro Regional:</h6>
                    <span class="text-secondary">{{$estudiante->centro}}</span>
                  </li>
                  <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                    <h6 class="mb-0"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-house" viewBox="0 0 16 16">
                        <path fill-rule="evenodd" d="M2 13.5V7h1v6.5a.5.5 0 0 0 .5.5h9a.5.5 0 0 0 .5-.5V7h1v6.5a1.5 1.5 0 0 1-1.5 1.5h-9A1.5 1.5 0 0 1 2 13.5zm11-11V6l-2-2V2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5z"/>
                        <path fill-rule="evenodd" d="M7.293 1.5a1 1 0 0 1 1.414 0l6.647 6.646a.5.5 0 0 1-.708.708L8 2.207 1.354 8.854a.5.5 0 1 1-.708-.708L7.293 1.5z"/>
                      </svg>  Facultad:</h6>
                    <span class="text-secondary">{{$estudiante->facultad}}</span>
                  </li>
                  <li class="list-group-item d-flex justify-content-between align-items-center flex-wrap">
                    <h6 class="mb-0"><i class="fa fa-graduation-cap" aria-hidden="true"></i>  Carrera:</h6>
                    <span class="text-secondary">{{$estudiante->carrera}}</span>
                  </li>
                </ul>
              </div>
            </div>
            <div class="col-md-8">


                


                


              <div class="card mb-3">
                  <div class="card-header">
                    <div class="row gutters-sm">
                        <div class="col-md-9 mb-3">
                            <div class="progress">
                                <div class="progress-bar" role="progressbar" style="width: {{($suma/60)*100}}%;" aria-valuenow="{{($suma/60)*100}}" aria-valuemin="0" aria-valuemax="100">{{round(($suma/60)*100, 1)}}%</div>
                            </div>
    
                        </div>
                        <div class="col-md-3">
                            <a class="btn btn-info " href="{{route("admin.constancias.show",$estudiante->id)}}" target="__blank" href="">
                                <i class="fa fa-download" aria-hidden="true"></i> Constancias</a>
    
    
                        </div>
    
                    </div>
                  </div>
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                          <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Dimensión</th>
                            <th scope="col">Fecha</th>
                            <th scope="col">Código</th>
                            <th scope="col">Horas</th>
                            <th scope="col">Estado</th>
                            
                          </tr>
                        </thead>
                        <tbody>

                            @foreach ($actividades as $actividad)
                            <tr @if ($actividad->estado=="Acreditado")class="table-success" @endif
                              @if ($actividad->estado=="No Asignado")class="table-danger" @endif
                              @if ($actividad->estado=="Pendiente")class="table-secondary" @endif >
                                <th scope="row">{{($loop->index+1)}}</th>
                                <td>{{$actividad->nombre}}</td>
                                <td>{{$actividad->dimension}}</td>
                                <td>{{$actividad->fecha}}</td>
                                <td>{{$actividad->codigo}}</td>
                                <td>{{$actividad->horas}}</td>
                                <td>{{$actividad->estado}}</td>
                                
                              </tr>
                        
                            @endforeach
                          
                          
                          <tr>
                            <td class="text-center"colspan="6"><Strong>Total</Strong></td>
                            <td class="text-center">{{$suma}}</td>
                          </tr>
                        </tbody>
                      </table>
                </div>
              </div>

            



            </div>
          </div>

        </div>
    </div>
@stop