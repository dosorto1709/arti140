@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Lista de Estudiantes</h1>
@stop

@section('content')
    @livewire('admin.constancia-index')
@stop
