<!DOCTYPE html>

<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>Document</title>
        <style>
        @font-face {
  font-family: 'Helvetica-Neue';
  font-style: normal;
  font-weight: normal;
  src: url(http://" . $_SERVER['SERVER_NAME']."/storage/images/helvetica.ttf) format('truetype');
}
        @page {
            size: 215.9mm  27.94cm !important;
            margin: 0px 0px 0px 0px !important;
            padding: 0px 0px 0px 0px !important;
        }
        .barraf {
            position: absolute;
            bottom: -133px;
            right: -5px;
        }
        .barra {
            position: absolute;
            top: 0px;
            right: -5px;
        }
        .infoCar {
            position: absolute;
            top: 30px;
            right: 35px;
            width: 300px;
        }
        .toop{
            position: absolute;
            top: -15px;
            

        }
        .toop2{
            position: absolute;
            top: 5px;
            left: 220px;

        }
        .inicio{
            position: absolute;
            top: 120px;
            left: 95px;
            right: 90px;

        }
        .lucen{
            bottom: 60px;
            position: absolute;
            right: 5px;

        }
        .lucent{
            width: 7.5cm;
            height: 12cm;
        }
        .centro{
            width: 6.3cm;
            height: 4cm;
            object-fit: cover;

        }
        .footer{
            bottom: 5px;
            position: absolute;
            right: 0px;
        }
        .logos{
            width: 100px;
            height: 100px;
            object-fit: cover;

        }
        .raya{
            margin: 0px 0px 0px 0px !important;
            padding: 0px 0px 0px 0px !important;
            font-size: 13px; 
            font-family: 'HelveticaNeue-Light', 'Helvetica Neue Light', 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif;
        }
        .colorDisable{
            color: rgb(171, 166, 166);
        }
        .fuente{
            font-size: 14px; 
            font-family: 'HelveticaNeue-Light', 'Helvetica Neue Light', 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif;

        }

        .italic {
        font-style: italic;
        }
        .info{
            margin: 0px 0px 0px 0px !important;
            padding: 0px 0px 0px 0px !important;
            font-size: 14px; 
            text-align: right; 
            font-family: 'HelveticaNeue-Light', 'Helvetica Neue Light', 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif;

        }
        .codigo{
            margin: 0px 0px 0px 0px !important;
            padding: 0px 0px 0px 0px !important;
            font-size: 12px; 
            bottom: 55px;
            position: absolute;
            left: 10px;
            font-family: 'HelveticaNeue-Light', 'Helvetica Neue Light', 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif;

        }
        .info2{
            margin: 0px 0px 0px 0px !important;
            padding: 0px 0px 0px 0px !important;
            font-size: 14px; 
            text-align: center; 
            font-family: 'HelveticaNeue-Light', 'Helvetica Neue Light', 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif;

        }
        
        div.vertical-line{
            display: flex;
            justify-content: flex-end;
            align-items:flex-end;
      background-color:#78252D;
      height: 100%;
      float: left;
    }
    </style>
    </head>
    <body>
        <div>

        </div>
        <div class="lucen">
            <img class="lucent"  src='{{public_path("/storage/images/lucen.png")}}' alt=""></img>

        </div>

        <div class="toop">
            <img  class="centro" src="{{public_path($logoCentro)}}" alt=""></img>
        </div>
        <div class="toop2">
            
            <img class="logos" src="{{public_path($logoCarrera)}}" alt=""></img>
        </div>
        <div class="infoCar">
            <p class="info"> <strong>{{(strtoupper($carrera))}}</strong> </p>
            <p class="info">Tel: <strong>{{($carreraTelefono)}}</strong></p>
            <p class="info">{{($carreraExt)}}</p>
        </div>
        <img class="barra" width="35px" height="120px" src='{{public_path("/storage/images/barrar.png")}}' alt=""></img>
        
        <div class="inicio">
            <p style="text-align: center" class="fuente italic">{{$anioEduc}}</p>
            
            <p style="text-align: center; font-size: 18px;" class="fuente">
                @if ($horas==60)
                    CONSTANCIA DE CUMPLIMIENTO DEL ARTICULO 140 DE LAS NORMAS ACADEMICAS
                @else
                    CONSTANCIA DE CUMPLIMIENTO DE {{$horaT}} ({{$horas}}) HORAS DE TRABAJO SOCIAL COMUNITARIO
                @endif</p>
            
            <p style="text-align: justify;" class="fuente" >
                Por este medio hago constar, que el (la) estudiante <b>{{strtoupper($nombre.' '.$apellido)}}</b>, con número de cuenta <b>{{$cuenta}}</b> inscrito(a) en la carrera de <b>{{$carrera}}</b>, participó en: 
            </p>
            
            <table border="1" style="font-size: 11px; text-align: center; vertical-align: middle;width: 100%; border-collapse: collapse">
                <thead>
                    <tr>
                        <th style='width: 5%;'>No</th>
                        <th style='width: 40%;'>ACTIVIDAD</th>
                        <th >DIMENSIÓN</th>
                        <th>CÓDIGO</th>
                        <th>HORAS ACREDITADAS</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($actividades as $actividad)
                    <tr>
                        <td>{{($loop->index+1)}}</td>
                        <td style="text-align: left;">{{$actividad->nombre}}</td>
                        <td>{{$actividad->dimension}}</td>
                        <td>{{$actividad->codigo}}</td>
                        <td>{{$actividad->horas}}</td>
                    </tr>
                        
                    @endforeach
                    
                    <tr>
                        <td colspan="4"> <b>TOTAL</b>  </td>
                        
                        <td  > <b>{{$suma}}</b> </td>
                    </tr>
                </tbody>
            </table>

 

            <p  style="text-align: justify;" class="fuente">
                Cumpliendo así las <b>{{$horas}}</b> horas de labor
                @if ($horas==60)
                exigidas en el artículo 140 de las normas académicas, numeral b. de la Universidad Nacional Autónoma de Honduras, establecido por el Consejo Universitario, según acuerdo 071-2004-CUO.
                @else
                exigidas en el trabajo Comunitario de la Universidad Nacional Autónoma de Honduras, establecido por el Consejo Universitario según acuerdo 071-2004-CUO.
                    
                @endif   
            </p>
            @php
                //es_HN
                //setlocale(LC_TIME, 'es_HN.UTF-8');
                setlocale(LC_ALL, 'es_ES');
                setlocale(LC_TIME, 'es_ES');
                $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
                $monthNum  = date('n');
                $dateObj   = DateTime::createFromFormat('!m', $monthNum);
                $monthName = strftime('%B', $dateObj->getTimestamp());
                $monthName = $meses[date('n')-1];
                //$monthName = strftime('%B', $page->getUnformatted('date'));
                
            @endphp
    
            <p style="text-align: justify;" class="fuente" >
                Y, para los fines que a el (la) interesado (a) estime conveniente, se extiende la presente constancia en {{$ciudad}}, Departamento de {{$depto}} a los {{date("d")}} días del mes de {{$monthName}} del año {{date("Y")}}.
            
            </p>

            <p style="text-align: center" class="fuente italic"><br></p>
            <p style="text-align: center" class="fuente italic"><br></p>
            

            <table style="text-align: center; vertical-align: text-top;width: 100%; border-collapse: collapse">
                <tbody>
                    <tr>
                        <td>
                            <p class="info2"><b>{{$cordinador}}</b></p>
                            <p class="info2"><b>Coordinador(a) de Carrera</b></p>
                            <p class="info2"><b>{{$carrera}}</b></p>
                            <p class="info2"><b>{{$centro}}-UNAH</b></p>
                        </td>
                        <td style="vertical-align: top;">
                            <p class="info2"><b>Vo. Bo. {{$voae}}</b></p>
                            <p class="info2"><b>Coordinador(a) VOAE-{{$centro}}</b></p>
                        </td>
                    </tr>
                </tbody>
            </table>

            <p class="fuente colorDisable">Cc: Archivo </p>
            <img src="data:image/png;base64, {!! $qrcode !!}">

        </div>
        <p class="codigo colorDisable">{{$codigo}} </p>

        


        <div class="footer">

        <img class="barraf" width="35px" height="80px" src='{{public_path("/storage/images/barran.png")}}' alt=""></img>

            <div>
                <p style="text-align: center" class="fuente italic">{{$proyecto}}</p>
                
                <p style="text-align: center" class="fuente">
                     <b class="raya">&mdash;</b><i class="raya colorDisable">&mdash;</i><b class="raya">&mdash;</b><i class="raya colorDisable">&mdash;</i><b class="raya">&mdash;</b><i class="raya colorDisable">&mdash;</i><b class="raya">&mdash;</b><i class="raya colorDisable">&mdash;</i><b class="raya">&mdash;</b><i class="raya colorDisable">&mdash;</i><b class="raya">&mdash;</b><i class="raya colorDisable">&mdash;</i><b class="raya">&mdash;</b><i class="raya colorDisable">&mdash;</i>
                     <b class="raya">&mdash;</b><i class="raya colorDisable">&mdash;</i><b class="raya">&mdash;</b><i class="raya colorDisable">&mdash;</i><b class="raya">&mdash;</b><i class="raya colorDisable">&mdash;</i><b class="raya">&mdash;</b><i class="raya colorDisable">&mdash;</i><b class="raya">&mdash;</b><i class="raya colorDisable">&mdash;</i><b class="raya">&mdash;</b><i class="raya colorDisable">&mdash;</i><b class="raya">&mdash;</b><i class="raya colorDisable">&mdash;</i>
                     <b class="raya">&mdash;</b><i class="raya colorDisable">&mdash;</i><b class="raya">&mdash;</b><i class="raya colorDisable">&mdash;</i><b class="raya">&mdash;</b><i class="raya colorDisable">&mdash;</i><b class="raya">&mdash;</b><i class="raya colorDisable">&mdash;</i><b class="raya">&mdash;</b><i class="raya colorDisable">&mdash;</i><b class="raya">&mdash;</b><i class="raya colorDisable">&mdash;</i><b class="raya">&mdash;</b><i class="raya colorDisable">&mdash;</i>
                     <b class="raya">&mdash;</b><i class="raya colorDisable">&mdash;</i><b class="raya">&mdash;</b><i class="raya colorDisable">&mdash;</i><b class="raya">&mdash;</b><i class="raya colorDisable">&mdash;</i><b class="raya">&mdash;</b><i class="raya colorDisable">&mdash;</i><b class="raya">&mdash;</b><i class="raya colorDisable">&mdash;</i><b class="raya">&mdash;</b><i class="raya colorDisable">&mdash;</i><b class="raya">&mdash;</b><i class="raya colorDisable">&mdash;</i>
                     <b class="raya">&mdash;</b><i class="raya colorDisable">&mdash;</i><b class="raya">&mdash;</b><i class="raya colorDisable">&mdash;</i>
                </p>
                <p style="text-align: center; font-size: 11px; " class="fuente">
                    <b>
                Universidad Nacional Autónoma de Honduras | {{$centro}} | {{$depto}} | {{$direccion}} | {{$url}}
                    </b>

                </p>
            </div>


        </div>
        
    </body>
    
</html>