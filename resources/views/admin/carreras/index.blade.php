@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Lista de Carreras</h1>
@stop

@section('content')
@if (session('info'))

<div class="alert alert-success" >
    <strong>{{session('info')}}</strong>

</div>
    
@endif
    <div class="card" >
        @can('admin.carreras.create')
        
        <div class="card-header" >
            <a class="btn btn-primary" href="{{route("admin.carreras.create")}}">Agregar Carrera </a>
        </div>
        @endcan
        <div class="card-body">
            <table class="table table-stripe" >
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Centro</th>
                        <th>Facultad</th>
                        <th colspan="2">Opciones</th>
                    </tr>

                </thead>
                <tbody>
                    @foreach ($carreras as $carrera)
                        <tr>
                            <td>{{$carrera->id}}</td>
                            <td>{{$carrera->nombre}}</td>
                            <td>{{$carrera->centro}}</td>
                            <td>{{$carrera->facultad}}</td>

                            <td width="10px" > 
                                @can('admin.carreras.edit', Model::class)
                                <a class="btn btn-primary btn-sm " href="{{route("admin.carreras.edit",$carrera)}}">Editar </a>  
                                @endcan
                                
                            </td>
                            <td width="10px" >
                                @can('admin.carreras.destroy')
                                <form action="{{route("admin.carreras.destroy",$carrera)}}" method="POST">
                                @csrf
                                @method("delete")
                                <button type="submit" class="btn btn-danger btn-sm" >Eliminar</button>

                            </form>
                            @endcan
                        </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

        </div>

    </div>
@stop