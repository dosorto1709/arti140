@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Editar Carrera</h1>
@stop

@section('content')
@if (session('info'))

<div class="alert alert-success" >
    <strong>{{session('info')}}</strong>

</div>
    
@endif
<div class="card">
    <div class="card-body">
        {!! Form::Model($carrera,['route'=>['admin.carreras.update',$carrera], 'autocomplete'=>'off','method'=>'put', 'files'=>true]) !!}

        <div class="form-group">
            {!! Form::label('nombre', 'Nombre de la Carrera') !!}
            {!! Form::text('nombre', null, ['class'=>'form-control','placeholder'=>'Ingrese el nombre de la Carrera']) !!}
            @error('nombre')
                <span class="text-danger" > {{$message}}</span>
            @enderror
        
        </div>
        <div class="form-group">
            {!! Form::label('centro_id', 'Centro') !!}
            {!! Form::select('centro_id',$centros,null,['class'=>'form-control','placeholder' => 'Seleccione...'])!!}
            @error('centro_id')
                <span class="text-danger" > {{$message}}</span>
            @enderror
        
        </div>
        <div class="form-group">
            {!! Form::label('facultad_id', 'Facultad') !!}
            {!! Form::select('facultad_id',$facultads,null,['class'=>'form-control','placeholder' => 'Seleccione...'])!!}
            @error('facultad_id')
                <span class="text-danger" > {{$message}}</span>
            @enderror
        
        </div>
        <div class="form-group">
            {!! Form::label('telefono', 'Teléfono de la Carrera') !!}
            {!! Form::text('telefono', null, ['class'=>'form-control','placeholder'=>'Ingrese el Teléfono de la Carrera']) !!}
            @error('telefono')
                <span class="text-danger" > {{$message}}</span>
            @enderror
        
        </div>
        <div class="form-group">
            {!! Form::label('ext', 'Extensión') !!}
            {!! Form::text('ext', null, ['class'=>'form-control','placeholder'=>'Ingrese la Extensión']) !!}
            @error('ext')
                <span class="text-danger" > {{$message}}</span>
            @enderror
        
        </div>
        <div class="form-group">
            {!! Form::label('cordinador', 'Nombre del/la Cordinador/ra de Carrera') !!}
            {!! Form::text('cordinador', null, ['class'=>'form-control','placeholder'=>'Ej. Lic. Juan López']) !!}
            @error('cordinador')
                <span class="text-danger" > {{$message}}</span>
            @enderror
        
        </div>




        <div class="row mb-3" >
            <div class="col" >
                <div class="image-wrapper">
                    <img id="picture"src="{{Storage::url($carrera->logo)}}" width="300" height="200" alt="">

                </div>
            </div>
            <div class="col">
                <div class="form-group" >
                    {!! Form::label('logo', "Logotipo de la Carrera ") !!}
                    {!! Form::file('logo', ['class'=>'form-control-file','accept'=>'image/*']) !!}
                </div>
                <p>Favor subir una imagen en buena calidad</p>
            </div>

        </div>
        {!! Form::submit("Actualizar Carrera", ['class'=>'btn btn-primary']) !!}


        {!! Form::close() !!}
    </div>
</div>
@stop
@section('js')
        <script> console.log('Hi!'); </script>
        <script>            
            document.getElementById("logo").addEventListener('change', cambiarImagen);
            function cambiarImagen(event){
                var file = event.target.files[0];

                var reader = new FileReader();
                reader.onload = (event) => {
                    document.getElementById("picture").setAttribute('src', event.target.result);
                };

                reader.readAsDataURL(file);
            }
        </script>    
@stop