@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Lista de Facultades</h1>
@stop

@section('content')
@if (session('info'))

<div class="alert alert-success" >
    <strong>{{session('info')}}</strong>

</div>
    
@endif
    <div class="card" >
        @can('admin.facultads.create')
            
        
        <div class="card-header" >
            <a class="btn btn-primary" href="{{route("admin.facultads.create")}}">Agregar Facultad </a>

        </div>
        @endcan
        <div class="card-body">
            <table class="table table-stripe" >
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Centro</th>
                        <th colspan="2">Opciones</th>
                    </tr>

                </thead>
                <tbody>
                    @foreach ($facultads as $facultad)
                        <tr>
                            <td>{{$facultad->id}}</td>
                            <td>{{$facultad->nombre}}</td>
                            <td>{{$facultad->centro}}</td>
                            <td width="10px" > 
                                @can('admin.facultads.edit')
                                    
                                
                                <a class="btn btn-primary btn-sm " href="{{route("admin.facultads.edit",$facultad)}}">Editar </a>
                                @endcan
                            </td>
                            <td width="10px" >
                                @can('admin.facultads.destroy')
                                    <form action="{{route("admin.facultads.destroy",$facultad)}}" method="POST">
                                        @csrf
                                        @method("delete")
                                        <button type="submit" class="btn btn-danger btn-sm" >Eliminar</button>

                                    </form>
                                @endcan
                        </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

        </div>

    </div>
@stop