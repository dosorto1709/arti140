@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Editar Actividad</h1>
@stop

@section('content')
@if (session('info'))

<div class="alert alert-success" >
    <strong>{{session('info')}}</strong>

</div>
    
@endif
    <div class="card">
        <div class="card-body">
            {!! Form::Model($actividad,['route'=>['admin.actividads.update',$actividad], 'autocomplete'=>'off','method'=>'put']) !!}
            <div class="form-group">
                {!! Form::label('nombre', 'Nombre de la Actividad') !!}
                {!! Form::text('nombre', null, ['class'=>'form-control','placeholder'=>'Ingrese el nombre del Centro']) !!}
                @error('nombre')
                    <span class="text-danger" > {{$message}}</span>
                @enderror
            
            </div>
            <div class="form-group">
                {!! Form::label('horas', 'Horas del Artículo 140') !!}
                {!! Form::number('horas', null, ['class'=>'form-control','placeholder'=>'Ej. 5']) !!}
                @error('horas')
                    <span class="text-danger" > {{$message}}</span>
                @enderror
            
            </div>
            <div class="form-group">
                {!! Form::label('fecha', 'Fecha de la Actividad') !!}
                {!! Form::date('fecha', new \DateTime($actividad->fecha), ['class'=>'form-control','placeholder'=>'Fecha de Ejecución']) !!}
                @error('fecha')
                    <span class="text-danger" > {{$message}}</span>
                @enderror
            
            </div>
            <div class="form-group">
                {!! Form::label('dimension', 'Dimensión:') !!}
                {!! Form::select('dimension',$dimensions,null,['class'=>'form-control','placeholder' => 'Seleccione...'])!!}
                @error('dimension')
                    <span class="text-danger" > {{$message}}</span>
                @enderror
            
            </div>
            

            

            {!! Form::submit("Actualizar", ['class'=>'btn btn-primary']) !!}


            {!! Form::close() !!}
        </div>
    </div>
@stop
