@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Actividades</h1>
@stop

@section('content')
    @livewire('admin.actividad-index')
@stop