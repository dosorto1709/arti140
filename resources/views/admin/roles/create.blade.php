@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Crear nuevo Rol</h1>
@stop

@section('content')
@if (session('info'))

<div class="alert alert-success" >
    <strong>{{session('info')}}</strong>

</div>
    
@endif
    <div class="card">
        <div class="card-body">
            {!! Form::open(['route'=>'admin.roles.store']) !!}
            <div class="form-group">
                {!! Form::label('name', 'Nombre del Rol') !!}
                {!! Form::text('name', null, ['class'=>'form-control','placeholder'=>'Ingrese el nombre del Rol']) !!}
                @error('name')
                    <span class="text-danger" > {{$message}}</span>
                @enderror
            </div>
            <h2 class="h3" >Lista de Permisos</h2>
            @foreach ($permisions as $permision)
            <div>
                <label >
                    {!! Form::checkbox('permissions[]', $permision->id, null, ['class'=>'mr-1']) !!}
                    {{$permision->description}}
                </label>
            </div>
                
            @endforeach
            {!! Form::submit("Crear Rol", ['class'=>'btn btn-primary']) !!}
            {!! Form::close() !!}
        </div>
    </div>
@stop