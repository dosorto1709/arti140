@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Asignar un rol</h1>
@stop

@section('content')
@if (session('info'))
<div class="alert alert-success">
    <strong>{{session('info')}}</strong>

</div>
    
@endif


    <div class="card">
        <div class="card-body">
            <p class="h5">Nombre:</p>
            <p class="form-control">{{$user->name}}</p>

            <h2 class="h5">Listado de Roles </h2>
            {!! Form::model($user, ['route'=>['admin.users.update',$user],'method'=>'put']) !!}
            
            <div class="form-group">
                {!! Form::label('centro_id', 'Centro:') !!}
                {!! Form::select('centro_id',$centros,null,['class'=>'form-control','placeholder' => 'Pick a size...'])!!}
                @error('centro_id')
                    <span class="text-danger" > {{$message}}</span>
                @enderror
            
            </div>
            <div class="form-group">
                {!! Form::label('facultad_id', 'Facultad:') !!}
                {!! Form::select('facultad_id',$facultads,null,['class'=>'form-control','placeholder' => 'Pick a size...'])!!}
                @error('facultad_id')
                    <span class="text-danger" > {{$message}}</span>
                @enderror
            
            </div>
            <div class="form-group">
                {!! Form::label('carrera_id', 'Carrera:') !!}
                {!! Form::select('carrera_id',$carreras,null,['class'=>'form-control','placeholder' => 'Pick a size...'])!!}
                @error('carrera_id')
                    <span class="text-danger" > {{$message}}</span>
                @enderror
            
            </div>
            
            @foreach ($roles as $rol)
            <div>
                <label >
                    {!! Form::checkbox('roles[]', $rol->id, null, ['class'=>'mr-1']) !!}
                    {{$rol->name}}

                </label>
            </div>
                
            @endforeach
            {!! Form::submit("Asignar Rol", ['class'=>'btn btn-primary mt-2']) !!}
        </div>
    </div>
@stop
