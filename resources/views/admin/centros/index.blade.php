@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Lista de Centros</h1>
@stop

@section('content')
@if (session('info'))

<div class="alert alert-success" >
    <strong>{{session('info')}}</strong>

</div>
    
@endif
    <div class="card" >
        @can('admin.centros.create', Model::class)
        <div class="card-header" >
            <a class="btn btn-primary" href="{{route("admin.centros.create")}}">Agregar Centro </a>
        </div>
            
        @endcan
        
        <div class="card-body">
            <table class="table table-stripe" >
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>nombre</th>
                        <th colspan="2">Opciones</th>
                    </tr>

                </thead>
                <tbody>
                    @foreach ($centros as $centro)
                        <tr>
                            <td>{{$centro->id}}</td>
                            <td>{{$centro->nombre}}</td>
                            <td width="10px" > 
                                @can('admin.centros.edit')
                                <a class="btn btn-primary btn-sm " href="{{route("admin.centros.edit",$centro)}}">Editar </a>
                                @endcan
                            </td>
                            <td width="10px" >
                                @can('admin.centros.destroy')
                                    <form action="{{route("admin.centros.destroy",$centro)}}" method="POST">
                                        @csrf
                                        @method("delete")
                                        <button type="submit" class="btn btn-danger btn-sm" >Eliminar</button>

                                    </form>
                                @endcan
                        </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

        </div>

    </div>
@stop