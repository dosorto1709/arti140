@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Editar Centro</h1>
@stop

@section('content')
@if (session('info'))

<div class="alert alert-success" >
    <strong>{{session('info')}}</strong>

</div>
    
@endif
<div class="card">
    <div class="card-body">
        {!! Form::Model($centro,['route'=>['admin.centros.update',$centro], 'autocomplete'=>'off','method'=>'put', 'files'=>true]) !!}
        <div class="form-group">
            {!! Form::label('nombre', 'Nombre del Centro') !!}
            {!! Form::text('nombre', null, ['class'=>'form-control','placeholder'=>'Ingrese el nombre del Centro']) !!}
            @error('nombre')
                <span class="text-danger" > {{$message}}</span>
            @enderror
        
        </div>
        <div class="form-group">
            {!! Form::label('depto', 'Nombre del Departamento') !!}
            {!! Form::text('depto', null, ['class'=>'form-control','placeholder'=>'Ingrese el nombre del Departamento']) !!}
            @error('depto')
                <span class="text-danger" > {{$message}}</span>
            @enderror
        
        </div>
        <div class="form-group">
            {!! Form::label('ciudad', 'Nombre de la ciudad') !!}
            {!! Form::text('ciudad', null, ['class'=>'form-control','placeholder'=>'Ingrese el nombre de la ciudad']) !!}
            @error('ciudad')
                <span class="text-danger" > {{$message}}</span>
            @enderror
        
        </div>
        <div class="form-group">
            {!! Form::label('voae', 'Nombre del/la Cordinador/ra Regional de VOAE') !!}
            {!! Form::text('voae', null, ['class'=>'form-control','placeholder'=>'Ej. Lic. María López']) !!}
            @error('voae')
                <span class="text-danger" > {{$message}}</span>
            @enderror
        
        </div>
        <div class="form-group">
            {!! Form::label('direccion', 'Dirección del Centro') !!}
            {!! Form::text('direccion', null, ['class'=>'form-control','placeholder'=>'No mas de 50 Carracteres']) !!}
            @error('direccion')
                <span class="text-danger" > {{$message}}</span>
            @enderror
        
        </div>
        <div class="form-group">
            {!! Form::label('url', 'Dirección URL o Sitio Web del Centro') !!}
            {!! Form::text('url', null, ['class'=>'form-control','placeholder'=>'www.curlp.unah.edu.hn']) !!}
            @error('url')
                <span class="text-danger" > {{$message}}</span>
            @enderror
        
        </div>

        <div class="row mb-3" >
            <div class="col" >
                <div class="image-wrapper">
                    <img id="picture"src="{{Storage::url($centro->logo)}}" width="300" height="200" alt="">

                </div>
            </div>
            <div class="col">
                <div class="form-group" >
                    {!! Form::label('logo', "Logotipo del centro ") !!}
                    {!! Form::file('logo', ['class'=>'form-control-file','accept'=>'image/*']) !!}
                </div>
                <p>Favor subir una imagen en buena calidad</p>
            </div>

        </div>
        {!! Form::submit("Actualizar Centro", ['class'=>'btn btn-primary']) !!}


        {!! Form::close() !!}
    </div>
</div>
@stop
@section('js')
        <script> console.log('Hi!'); </script>
        <script>            
            document.getElementById("logo").addEventListener('change', cambiarImagen);
            function cambiarImagen(event){
                var file = event.target.files[0];

                var reader = new FileReader();
                reader.onload = (event) => {
                    document.getElementById("picture").setAttribute('src', event.target.result);
                };

                reader.readAsDataURL(file);
            }
        </script>    
@stop
