
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
		<title></title>
		<style>
            @media print {
    .pagebreak { page-break-before: always; } /* page-break-after works, as well */
}




.cupon{
background: #26c6da;
background: -webkit-linear-gradient(to right, #002C9E, #000E33); 
background: linear-gradient(to right, #002C9E, #000E33);
 width:auto;
 border-radius: 7px;
  padding: 1rem;
  color: #fff;
  font-family: 'Tahoma', sans-serif;
  
  margin: 0.2rem;
}

.titulo{
  color: rgba(255,255,255,0.75);
  font-weight: 600;
  margin-bottom: 1rem;
  font-size: 8px
}



.code{
  padding: 0.3rem 0.9rem;
  background: #fff;
  border: none;
  border-radius: 30px;
  color: rgba(0,0,0,255);
  font-size: 16px;
  font-weight: 600;
  margin-top: 2rem;
  cursor: pointer;
  outline: none;
  transition: 250ms
}
.code:hover{box-shadow: 0 2px 10px rgba(0,0,0,0.25)}
.cuerpo{
  padding-bottom: 1rem;
  border-bottom: 2px dashed rgba(255,255,255,255);
  
}



		</style>
	</head>
	<body onload="window.print()">

        <div class="container-fluid">
            <div class="row flex-row ">

                @foreach ($codigos as $codigo)
                <div class="col-3">
                    <div class="card card-block">
                        
                        <div class="card-body cupon text-center">
                            <div class="cuerpo text-center" >
                            <h5 class='titulo'> {{env('APP_URL')}}

                             </h5>
                           <h1 class="display-4" > <b>{{$codigo->horas}}</b> </h1>
                           <h3>Horas</h3>
                                
                            </div>
                            <button class='code'> {{$codigo->codigo}} </button>
                        </div>
                    </div>
                    @if (($loop->index+1)==12 or ($loop->index+1)==24 or ($loop->index+1)==36 or ($loop->index+1)==48 or ($loop->index+1)==60 or($loop->index+1)==72 or ($loop->index+1)==84 or ($loop->index+1)==96 or ($loop->index+1)==108 or ($loop->index+1)==120 )
                    <div style='page-break-after:always'></div>
                    @endif
                    
                </div>
                    
                @endforeach
                
                
            </div>
        </div>

	</body>
</html>




