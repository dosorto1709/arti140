@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Generación de Códigos</h1>
@stop

@section('content')
    @livewire('admin.code-index')
@stop