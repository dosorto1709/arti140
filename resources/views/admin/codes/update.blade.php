@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Validar códigos acreditados</h1>
@stop

@section('content')
<div class="card">
    @if (session('info'))

    <div class="alert alert-success" >
        <strong>{{session('info')}}</strong>

    </div>
        
    @endif
    
    <div class="card-body">
        <div class="form-group">
            <p class="h5">Actividad:</p>
            <p class="form-control">{{$actividad->nombre}}</p>
        </div>
        <div class="form-group">
            <p class="h5">Fecha:</p>
            <p class="form-control">{{$actividad->fecha}}</p>
        </div>
        @can('admin.validars.create', Model::class)
        <div class="card-header text-right" >
            <form action="{{route("admin.validars.update",$actividad->id)}}" method="POST">
                @csrf
                @method("put")
                <button type="submit" class="btn btn-primary btn-sm" >Acreditar todos</button>
            </form>
        </div>
    @endcan

        <table class="table table-stripe">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Codigo</th>
                    <th>Estado</th>
                    <th>Estudiante</th>
                    <th>Cuenta</th>
                    <th>Carrera</th>
                    <th colspan="2">Opciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($codes as $code)
                <tr @if ($code->estado=="Acreditado")class="table-success" @endif
                @if ($code->estado=="No Asignado")class="table-danger" @endif
                @if ($code->estado=="Pendiente")class="table-secondary" @endif
                
                >
                    <td>{{$code->id}}</td>
                    <td>{{$code->codigo}}</td>
                    <td>{{$code->estado}}</td>
                    <td>{{$code->nombre.' '.$code->apellido}}</td>
                    <td>{{$code->cuenta}}</td>
                    <td>{{$code->carrera}}</td>
                    <td  >
                        
                        @can('admin.validars.edit')
                                <a class="btn btn-success btn-sm " href="{{route("admin.validars.show",$code)}}">Acreditar </a>
                        @endcan
                    </td>
                    <td  >
                        @can('admin.validars.destroy')
                        <form action="{{route("admin.validars.destroy",$code)}}" method="POST">
                            @csrf
                            @method("delete")
                            <button type="submit" class="btn btn-danger btn-sm" >No Acreditar</button>

                        </form>
                        @endcan
                    
                    </td>
                </tr>
                    
                @endforeach

            </tbody>
        </table>

        
     
    </div>
</div>
@stop
