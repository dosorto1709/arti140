@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Crear Códigos</h1>
@stop

@section('content')
@if (session('info'))

<div class="alert alert-success" >
    <strong>{{session('info')}}</strong>

</div>
    
@endif
<div class="card">
    <div class="card-body">
        {!! Form::open(['route'=>['admin.codes.store'], 'autocomplete'=>'off']) !!}
        <input type="text" hidden name="actividad_id" value="{{$actividad->id}}">
        <div class="form-group">
            <p class="h5">Actividad:</p>
            <p class="form-control">{{$actividad->nombre}}</p>
        </div>
        <div class="form-group">
            <p class="h5">Fecha:</p>
            <p class="form-control">{{$actividad->fecha}}</p>
        </div>

        

        <div class="form-group">
            {!! Form::label(null, 'Si desea crea un número ilimitado de códigos marque la siguiente casilla') !!}
            <br/>
            <label >
                
                {!! Form::checkbox('infinito', null, false, ['class'=>'mr-2']) !!}
                 Ilimitados

            </label>
        </div>

        <div id="content" class="form-group">
            {!! Form::label('cantidad', 'Cantidad') !!}
            {!! Form::number('cantidad', null, ['class'=>'form-control','placeholder'=>'Ej. 10']) !!}
            @error('cantidad')
                <span class="text-danger" > {{$message}}</span>
            @enderror
        
        </div>
        
        
        

        
        {!! Form::submit("Generar Códigos", ['class'=>'btn btn-primary']) !!}


        {!! Form::close() !!}
    </div>
</div>
@stop
@section('js')
        <script>   

            var checkbox = document.querySelector("input[name=infinito]");

            checkbox.addEventListener('change', function() {
                element = document.getElementById("content");
            if (this.checked) {
                element.style.display='none';
            } else {
                element.style.display='block';
            }
            });         
            
        </script>    
        
@stop
