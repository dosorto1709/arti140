@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Códigos de Actividad</h1>
@stop

@section('content')
<div class="card">
    @if (session('info'))

    <div class="alert alert-success" >
        <strong>{{session('info')}}</strong>

    </div>
        
    @endif
    <div class="card-body">
        <div class="form-group">
            <p class="h5">Actividad:</p>
            <p class="form-control">{{$actividad->nombre}}</p>
        </div>
        <div class="form-group">
            <p class="h5">Fecha:</p>
            <p class="form-control">{{$actividad->fecha}}</p>
        </div>

        <div class="card-header text-right" >
            <form target="__blank" action="{{route("admin.validars.index",$actividad->id)}}" method="GET">
                @csrf
                @method("get")
                <input type="text" hidden name="id" value="{{$actividad->id}}">
                <button type="submit" class="btn btn-primary btn-sm" >Imprimir disponibles</button>
            </form>
        </div>

        <table class="table table-stripe">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Código</th>
                    <th>Tomado/Desactivado</th>
                    <th>Ilimitado</th>
                    <th colspan="1">Opciones</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($codes as $code)
                <tr @if ($code->usado==0)class="table-success" @endif
                    @if ($code->usado==1)class="table-secondary" @endif >
                    <td>{{$code->id}}</td>
                    <td>{{$code->codigo}}</td>
                    <td>@if ($code->usado)
                        SI
                    @else
                        NO
                    @endif </td>
                    <td>@if ($code->infinito)
                        SI
                    @else
                        NO
                    @endif
                     </td>
                    <td WIDTH="30" >
                        @if ($code->usado===0)
                        @can('admin.codes.update')
                        <form action="{{route("admin.codes.destroy",$code)}}" method="POST">
                            @csrf
                            @method("delete")
                            <button type="submit" class="btn btn-danger btn-sm" >Desactivar</button>

                        </form>
                        @endcan
                            
                        @endif
                    </td>
                </tr>
                    
                @endforeach

            </tbody>
        </table>

        
     
    </div>
</div>
@stop

