<x-app-layout>
    {!! Form::open(['route'=>'registro','autocomplete'=>'off','files'=>true]) !!}
    <div class="max-w-[85rem] mx-auto px-4 sm:px-6 lg:px-8 py-20 px-4 py-5 bg-white space-y-6 sm:p-6" >
        <div class="hidden sm:block" aria-hidden="true">
            <div class="py-5">
              <div class="border-t border-gray-200"></div>
            </div>
          </div>
        <div class="md:grid md:grid-cols-3 md:gap-6">
          <div class="md:col-span-1">
            <div class="px-4 sm:px-0">
              <h3 class="text-2xl font-medium leading-6 text-gray-900">Registro Estudiantil</h3>
              <p class="mt-1 text-xl text-gray-600">
                Para acumular horas del Artículo 140 de las Normas Académicas de la UNAH, debe registrarte en el siguiente formulario, recuerda agregar información real de lo contrario tu registro no será valido  
              </p>
            </div>
          </div>
    
          <div class="mt-5 md:mt-0 md:col-span-2">
            <div class="">
                <div>
                    <h1 class="text-xl" >Nuevo registro para cuenta: {{$cuenta}}</h1>
                    {!! Form::text('cuenta', $cuenta, ['hidden'=>true]) !!}
                </div>
                <div class="md:grid md:grid-cols-2 md:gap-6">


                    <div class="px-4 py-5 bg-white space-y-6 sm:p-6">
                        <div class="grid grid-cols-3 gap-6">
                            <div class="col-span-3 sm:col-span-2">
                            
                            <div class="mt-1 text-center ">
                                <img style="display: inline-block;
                                position: relative;
                                width: 150px;
                                height: 150px;
                                object-fit: cover;
                                overflow: hidden;
                                border-radius: 50%;" id="picture"src="{{Storage::url('public/images/user.png')}}" alt="">
                            </div>
                            </div>
                        </div>
                        @error('nombre')
                        <div  class="text-black px-6 py-4 border-0 rounded relative mb-4 bg-red-500">
                            
                            <span class="inline-block align-middle mr-8">
                            <b class="capitalize">Atención</b> Campo requerido
                            </span>
                        </div>
                        @enderror
                    
                    </div>

                    <div class="px-4 py-5 bg-white space-y-6 sm:p-6">
                        <div class="form-group" >
                            {!! Form::label('foto', "Fotografia tipo Carnet ") !!}
                            {!! Form::file('foto', ['class'=>'form-control-file','accept'=>'image/*']) !!}
                        </div>
                        <p><Strong>ATENCIÓN!! <br/> </Strong>
                            <Strong>Esta fotografia no podrá ser modificada</Strong>
                        </p>
                        
                    
                    </div>
                </div>
                <div class="md:grid md:grid-cols-2 md:gap-6">


                    <div class="px-4 py-5 bg-white space-y-6 sm:p-6">
                        <div class="grid grid-cols-3 gap-6">
                            <div class="col-span-3 sm:col-span-2">
                            <label class="block text-sm font-medium text-gray-700">
                                Nombre
                            </label>
                            <div class="mt-1 flex rounded-md shadow-sm">
                                <span class="inline-flex items-center px-3 rounded-l-md border border-r-0 border-gray-300 bg-gray-50 text-gray-500 text-sm">
                                    <svg class="h-4 w-4 text-blue-500"  viewBox="0 0 24 24"  fill="none"  stroke="currentColor"  stroke-width="2"  stroke-linecap="round"  stroke-linejoin="round">  <path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2" />  <circle cx="12" cy="7" r="4" /></svg>
                                </span>
                                {!! Form::text('nombre', null, ['class'=>'focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded-none rounded-r-md sm:text-sm border-gray-300','placeholder'=>'Ej. María Andrea']) !!}
                            </div>
                            </div>
                        </div>
                        @error('nombre')
                        <div  class="text-black px-6 py-4 border-0 rounded relative mb-4 bg-red-500">
                            
                            <span class="inline-block align-middle mr-8">
                            <b class="capitalize">Atención</b> Campo requerido
                            </span>
                        </div>
                        @enderror
                    
                    </div>

                    <div class="px-4 py-5 bg-white space-y-6 sm:p-6">
                        <div class="grid grid-cols-3 gap-6">
                            <div class="col-span-3 sm:col-span-2">
                            <label class="block text-sm font-medium text-gray-700">
                                Apellido:
                            </label>
                            <div class="mt-1 flex rounded-md shadow-sm">
                                <span class="inline-flex items-center px-3 rounded-l-md border border-r-0 border-gray-300 bg-gray-50 text-gray-500 text-sm">
                                    <svg class="h-4 w-4 text-blue-500"  viewBox="0 0 24 24"  fill="none"  stroke="currentColor"  stroke-width="2"  stroke-linecap="round"  stroke-linejoin="round">  <path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2" />  <circle cx="12" cy="7" r="4" /></svg>
                                </span>
                                {!! Form::text('apellido', null, ['class'=>'focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded-none rounded-r-md sm:text-sm border-gray-300','placeholder'=>'Ej. Flores Castillo']) !!}
                            </div>
                            </div>
                        </div>
                        @error('apellido')
                        <div  class="text-black px-6 py-4 border-0 rounded relative mb-4 bg-red-500">
                            
                            <span class="inline-block align-middle mr-8">
                            <b class="capitalize">Atención</b> Campo requerido
                            </span>
                        </div>
                        @enderror
                    
                    </div>
                </div>

                <div class="md:grid md:grid-cols-2 md:gap-6">


                    <div class="px-4 py-5 bg-white space-y-6 sm:p-6">
                        <div class="grid grid-cols-3 gap-6">
                            <div class="col-span-3 sm:col-span-2">
                            <label class="block text-sm font-medium text-gray-700">
                                Teléfono
                            </label>
                            <div class="mt-1 flex rounded-md shadow-sm">
                                <span class="inline-flex items-center px-3 rounded-l-md border border-r-0 border-gray-300 bg-gray-50 text-gray-500 text-sm">
                                    <svg class="h-4 w-4 text-blue-500"  viewBox="0 0 24 24"  fill="none"  stroke="currentColor"  stroke-width="2"  stroke-linecap="round"  stroke-linejoin="round">  <path d="M22 16.92v3a2 2 0 0 1-2.18 2 19.79 19.79 0 0 1-8.63-3.07 19.5 19.5 0 0 1-6-6 19.79 19.79 0 0 1-3.07-8.67A2 2 0 0 1 4.11 2h3a2 2 0 0 1 2 1.72 12.84 12.84 0 0 0 .7 2.81 2 2 0 0 1-.45 2.11L8.09 9.91a16 16 0 0 0 6 6l1.27-1.27a2 2 0 0 1 2.11-.45 12.84 12.84 0 0 0 2.81.7A2 2 0 0 1 22 16.92z" /></svg>                                </span>
                                {!! Form::text('telefono', null, ['class'=>'focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded-none rounded-r-md sm:text-sm border-gray-300','placeholder'=>'Ej. 8845-6734']) !!}
                            </div>
                            </div>
                        </div>
                        @error('telefono')
                        <div  class="text-black px-6 py-4 border-0 rounded relative mb-4 bg-red-500">
                            
                            <span class="inline-block align-middle mr-8">
                            <b class="capitalize">Atención</b> Campo requerido
                            </span>
                        </div>
                        @enderror
                    
                    </div>

                    <div class="px-4 py-5 bg-white space-y-6 sm:p-6">
                        <div class="grid grid-cols-3 gap-6">
                            <div class="col-span-3 sm:col-span-2">
                            <label class="block text-sm font-medium text-gray-700">
                                Correo Institucional:
                            </label>
                            <div class="mt-1 flex rounded-md shadow-sm">
                                <span class="inline-flex items-center px-3 rounded-l-md border border-r-0 border-gray-300 bg-gray-50 text-gray-500 text-sm">
                                    <svg class="h-4 w-4 text-blue-500"  width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">  <path stroke="none" d="M0 0h24v24H0z"/>  <rect x="3" y="5" width="18" height="14" rx="2" />  <polyline points="3 7 12 13 21 7" /></svg>                                </span>
                                {!! Form::text('correo', null, ['class'=>'focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded-none rounded-r-md sm:text-sm border-gray-300','placeholder'=>'Ej. jpaz@edu.hn']) !!}
                            </div>
                            </div>
                        </div>
                        @error('correo')
                        <div  class="text-black px-6 py-4 border-0 rounded relative mb-4 bg-red-500">
                            
                            <span class="inline-block align-middle mr-8">
                            <b class="capitalize">Atención</b> Campo requerido
                            </span>
                        </div>
                        @enderror
                    
                    </div>
                </div>

                <div class="md:grid md:grid-cols-2 md:gap-6">


                    <div class="px-4 py-5 bg-white space-y-6 sm:p-6">
                        <div class="grid grid-cols-3 gap-6">
                            <div class="col-span-3 sm:col-span-2">
                            <label class="block text-sm font-medium text-gray-700">
                                Centro Regional
                            </label>
                            <div class="mt-1 flex rounded-md shadow-sm">
                                <span class="inline-flex items-center px-3 rounded-l-md border border-r-0 border-gray-300 bg-gray-50 text-gray-500 text-sm">
                                    <svg class="h-4 w-4 text-blue-500"  width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">  <path stroke="none" d="M0 0h24v24H0z"/>  <path d="M8 9l5 5v7h-5v-4m0 4h-5v-7l5 -5m1 1v-6a1 1 0 0 1 1 -1h10a1 1 0 0 1 1 1v17h-8" />  <line x1="13" y1="7" x2="13" y2="7.01" />  <line x1="17" y1="7" x2="17" y2="7.01" />  <line x1="17" y1="11" x2="17" y2="11.01" />  <line x1="17" y1="15" x2="17" y2="15.01" /></svg>
                                                                </span>
                                {!! Form::select('centro_id',$centros,null,['class'=>'focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded-none rounded-r-md sm:text-sm border-gray-300','placeholder' => 'Seleccione...'])!!}
                            </div>
                            </div>
                        </div>
                        @error('centro_id')
                        <div  class="text-black px-6 py-4 border-0 rounded relative mb-4 bg-red-500">
                            
                            <span class="inline-block align-middle mr-8">
                            <b class="capitalize">Atención</b> Campo requerido
                            </span>
                        </div>
                        @enderror
                    
                    </div>

                    <div class="px-4 py-5 bg-white space-y-6 sm:p-6">
                        <div class="grid grid-cols-3 gap-6">
                            <div class="col-span-3 sm:col-span-2">
                            <label class="block text-sm font-medium text-gray-700">
                                Facultad/Departamento Academico:
                            </label>
                            <div class="mt-1 flex rounded-md shadow-sm">
                                <span class="inline-flex items-center px-3 rounded-l-md border border-r-0 border-gray-300 bg-gray-50 text-gray-500 text-sm">
                                    <svg class="h-4 w-4 text-blue-500"  width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">  <path stroke="none" d="M0 0h24v24H0z"/>  <line x1="3" y1="21" x2="21" y2="21" />  <path d="M5 21v-14l8 -4v18" />  <path d="M19 21v-10l-6 -4" />  <line x1="9" y1="9" x2="9" y2="9.01" />  <line x1="9" y1="12" x2="9" y2="12.01" />  <line x1="9" y1="15" x2="9" y2="15.01" />  <line x1="9" y1="18" x2="9" y2="18.01" /></svg>                                 </span>
                                    {!! Form::select('facultad_id',$facultads,null,['class'=>'focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded-none rounded-r-md sm:text-sm border-gray-300','placeholder' => 'Seleccione...'])!!}
                                </div>
                            </div>
                        </div>
                        @error('facultad_id')
                        <div  class="text-black px-6 py-4 border-0 rounded relative mb-4 bg-red-500">
                            
                            <span class="inline-block align-middle mr-8">
                            <b class="capitalize">Atención</b> Campo requerido
                            </span>
                        </div>
                        @enderror
                    
                    </div>
                </div>

                <div class="md:grid md:grid-cols-2 md:gap-6">


                    <div class="px-4 py-5 bg-white space-y-6 sm:p-6">
                        <div class="grid grid-cols-3 gap-6">
                            <div class="col-span-3 sm:col-span-2">
                            <label class="block text-sm font-medium text-gray-700">
                                Carrera
                            </label>
                            <div class="mt-1 flex rounded-md shadow-sm">
                                <span class="inline-flex items-center px-3 rounded-l-md border border-r-0 border-gray-300 bg-gray-50 text-gray-500 text-sm">
                                    <svg class="h-4 w-4 text-blue-500"  width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">  <path stroke="none" d="M0 0h24v24H0z"/>  <path d="M8 9l5 5v7h-5v-4m0 4h-5v-7l5 -5m1 1v-6a1 1 0 0 1 1 -1h10a1 1 0 0 1 1 1v17h-8" />  <line x1="13" y1="7" x2="13" y2="7.01" />  <line x1="17" y1="7" x2="17" y2="7.01" />  <line x1="17" y1="11" x2="17" y2="11.01" />  <line x1="17" y1="15" x2="17" y2="15.01" /></svg>
                                                                </span>
                                {!! Form::select('carrera_id',$carreras,null,['class'=>'focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded-none rounded-r-md sm:text-sm border-gray-300','placeholder' => 'Seleccione...'])!!}
                            </div>
                            </div>
                        </div>
                        @error('carrera_id')
                        <div  class="text-black px-6 py-4 border-0 rounded relative mb-4 bg-red-500">
                            
                            <span class="inline-block align-middle mr-8">
                            <b class="capitalize">Atención</b> Campo requerido
                            </span>
                        </div>
                        @enderror
                    
                    </div>
                    <div class="px-2 py-8 bg-gray-50 text-center sm:px-6">
                        {!! Form::submit("Registrarme", ['class'=>'inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500']) !!}
                      </div>

                    
                </div>

                
    
                
    
    
                
            </div>
        </div>
    
          
        </div>
      </div>
      {!! Form::close() !!}

      <script>            
        document.getElementById("foto").addEventListener('change', cambiarImagen);
        function cambiarImagen(event){
            var file = event.target.files[0];

            var reader = new FileReader();
            reader.onload = (event) => {
                document.getElementById("picture").setAttribute('src', event.target.result);
            };

            reader.readAsDataURL(file);
        }
    </script>   
</x-app-layout>