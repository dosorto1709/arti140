<x-app-layout>

  <div class="max-w-[85rem] mx-auto px-4 sm:px-6 lg:px-8 py-20 px-4 py-5 bg-white space-y-6 sm:p-6" >
    <div class="hidden sm:block" aria-hidden="true">
        <div class="py-5">
          <div class="border-t border-gray-200"></div>
        </div>
    </div>
    
    
    @if(Session::has('info'))
    <div class="text-center">
      <strong>Atención</strong>
        <strong>{{session('info')}}</strong>

    </div>
        
    @endif

    



        <div class="md:grid md:grid-cols-3 md:gap-6">
          <div class="md:col-span-1">
            <div class="text-center" >
              <div class="text-center">
                <img style="display: inline-block;
                          position: relative;
                          width: 90px;
                          height: 90px;
                          object-fit: cover;
                          overflow: hidden;
                          border-radius: 50%;"  src="{{Storage::url($estudiante->foto)}}"  />
                <table WIDTH="100%" class="leading-normal">
                
                <tbody>
                    <tr class="w-full">
                        <td class="border-b border-gray-200 bg-white text-sm ">
                          <p class="text-gray-900  text-center">Nombre:</p>
                        </td>
                        <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                            <p class="text-gray-900 text-center">{{$estudiante->nombre.' '.$estudiante->apellido}}</p>
                        </td>
                    </tr>
                    <tr>
                      <td class="border-b border-gray-200 bg-white text-sm ">
                        <p class="text-gray-900  text-center">Cuenta:</p>
                      </td>
                      <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                          <p class="text-gray-900 text-center">{{$estudiante->cuenta}}</p>
                      </td>
                  </tr>
                  <tr>
                    <td class="border-b border-gray-200 bg-white text-sm ">
                      <p class="text-gray-900  text-center">Correo:</p>
                    </td>
                    <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                        <p class="text-gray-900 text-center">{{$estudiante->correo}}</p>
                    </td>
                </tr>
                <tr>
                  <td class="border-b border-gray-200 bg-white text-sm ">
                    <p class="text-gray-900  text-center">Centro:</p>
                  </td>
                  <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                      <p class="text-gray-900 text-center">{{$estudiante->centro}}</p>
                  </td>
              </tr>
              <tr>
                <td class="border-b border-gray-200 bg-white text-sm ">
                  <p class="text-gray-900  text-center">Facultad:</p>
                </td>
                <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                    <p class="text-gray-900 text-center">{{$estudiante->facultad}}</p>
                </td>
            </tr>
            <tr>
              <td class="border-b border-gray-200 bg-white text-sm ">
                <p class="text-gray-900  text-center">Carrera:</p>
              </td>
              <td class="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                  <p class="text-gray-900 text-center">{{$estudiante->carrera}}</p>
              </td>
          </tr>
                    
                </tbody>
            </table>
                
              </div>

              

            

            </div>


            <div class="md:grid md:grid-cols-3 ">
              
              <div class="md:col-span-1">
                <div class="mr-4 p-3 text-center">
                  <img style="display: inline-block;
                            position: relative;
                            width: 150px;
                            height: 80px;
                            object-fit: cover;
                            overflow: hidden;
                            border-radius: 10%;"  src="{{Storage::url($estudiante->logoCentro)}}"  />
                </div>

          </div>
          <div class="md:col-span-1">
            <div class="mr-4 p-3 text-center">
              <img style="display: inline-block;
                    position: relative;
                    width: 80px;
                    height: 80px;
                    object-fit: cover;
                    overflow: hidden;
                    border-radius: 50%;" src="{{Storage::url($estudiante->logoFacultad)}}"  />
            </div>

            
          </div>
          <div class="md:col-span-1">
            <div class="mr-4 p-3 text-center">
              <img style="display: inline-block;
                    position: relative;
                    width: 80px;
                    height: 80px;
                    object-fit: cover;
                    overflow: hidden;
                    border-radius: 50%;" src="{{Storage::url($estudiante->logoCarrera)}}"  />
            </div>
            
          </div>


        </div>

      </div>

      <div class="md:col-span-2">
      <p>Sí usted participó en una actividad que acumula horas del artículo 140 y le propocionaron un código, favor ingresarlo a continuación:  </p>

        {!! Form::open(['route'=>'acumular','autocomplete'=>'off','method'=>'post']) !!}

        <div class="md:grid md:grid-cols-2 md:gap-6">

          <div class="md:col-span-1">


            <div class="px-4 py-5 bg-white space-y-6 sm:p-6">
              <div class="grid grid-cols-3 gap-6">
                  <div class="col-span-3 sm:col-span-2">
                  <label class="block text-sm font-medium text-gray-700">
                      Código:
                  </label>
                  <div class="mt-1 flex rounded-md shadow-sm">
                      <span class="inline-flex items-center px-3 rounded-l-md border border-r-0 border-gray-300 bg-gray-50 text-gray-500 text-sm">
                        <svg class="h-6 w-6 text-blue-500"  width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">  <path stroke="none" d="M0 0h24v24H0z"/>  <path d="M4 7v-1a2 2 0 0 1 2 -2h2" />  <path d="M4 17v1a2 2 0 0 0 2 2h2" />  <path d="M16 4h2a2 2 0 0 1 2 2v1" />  <path d="M16 20h2a2 2 0 0 0 2 -2v-1" />  <rect x="5" y="11" width="1" height="2" />  <line x1="10" y1="11" x2="10" y2="13" />  <rect x="14" y="11" width="1" height="2" />  <line x1="19" y1="11" x2="19" y2="13" /></svg>
                      </span>
                      {!! Form::text('id', $estudiante->id, ['hidden'=>true]) !!}
                      {!! Form::text('codigo', null, ['class'=>'focus:ring-indigo-500 focus:border-indigo-500 flex-1 block w-full rounded-none rounded-r-md sm:text-sm border-gray-300','placeholder'=>'Ingrese un código de Actividad']) !!}
                  </div>
                  </div>
              </div>
              @error('nombre')
              <div  class="text-black px-6 py-4 border-0 rounded relative mb-4 bg-red-500">
                  
                  <span class="inline-block align-middle mr-8">
                  <b class="capitalize">Atención</b> Campo requerido
                  </span>
              </div>
              @enderror
          
          </div>


          </div>
          <div class="md:col-span-1">
            <div class="px-1 py-12  text-lefth">
              {!! Form::submit("Registrar", ['class'=>'inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500']) !!}
            </div>
            
          </div>
          
        </div>
        {!! Form::close() !!}

        <h1>
          <b>TOTAL HORAS ACUMULADAS:  {{$suma}}</b>
          
        </h1>
        <br>

        <div class="card">
          @if ($actividades->count())
              
          
          <div class="card-body">
             
              <table WIDTH="100%"  class="min-w-full leading-normal">
                  <thead>
                      <tr>
                          
                          <th class=" border-b-2 border-gray-200 bg-gray-100 text-center text-xs font-semibold text-gray-600 uppercase tracking-wider">Actividad</th>
                          <th class=" border-b-2 border-gray-200 bg-gray-100 text-center text-xs font-semibold text-gray-600 uppercase tracking-wider">Fecha</th>
                          <th class="border-b-2 border-gray-200 bg-gray-100 text-center text-xs font-semibold text-gray-600 uppercase tracking-wider">Dimensión</th>
                          <th class=" border-b-2 border-gray-200 bg-gray-100 text-center text-xs font-semibold text-gray-600 uppercase tracking-wider">Código de Registro</th>
                          <th class=" border-b-2 border-gray-200 bg-gray-100 text-center text-xs font-semibold text-gray-600 uppercase tracking-wider">Horas </th>
                          <th class=" border-b-2 border-gray-200 bg-gray-100 text-center text-xs font-semibold text-gray-600 uppercase tracking-wider">Estado </th>
                      </tr>
                  </thead>
                  <tbody>
                      @foreach ($actividades as $actividad)
                      
                      <tr @if ($actividad->estado=="Acreditado") style="background-color:#81c784" @endif
                        @if ($actividad->estado=="No Asignado")style="background-color:#e57373" @endif
                        @if ($actividad->estado=="Pendiente")style="background-color:#90caf9" @endif>
                          <td class="text-center border-b border-gray-200 bg-green-300 text-sm w-2/5">{{$actividad->nombre}}</td>
                          <td class="text-center border-b border-gray-200  text-sm w-2/5">{{$actividad->fecha}}</td>
                          <td class="text-center border-b border-gray-200  text-sm w-2/5">{{$actividad->dimension}}</td>
                          <td class="text-center border-b border-gray-200  text-sm w-2/5">{{$actividad->codigo}}</td>
                          <td class="text-center border-b border-gray-200  text-sm w-2/5">{{$actividad->horas}}</td>
                          <td class="text-center border-b border-gray-200  text-sm w-2/5">{{$actividad->estado}}</td>
  
                      </tr>
                          
                      @endforeach
  
                  </tbody>
              </table>
          </div>
          
              
          @else
          <div class="card-body">
            <div style="text-align: center">
              <strong>Aún no se han registrado actividades que acumulen horas del artículo 140</strong>
              <p>Puede avocarse a la coordinación academica de su carrera para que le acredite actividades en las que ha trabajado o le proporcione un código el cual podrá ingresar en la parte superior.</p>

            </div>
          </div>
          @endif
      </div>



      </div>



      <div class="md:col-span-1"> 

      </div>


    </div>


  </div>

        
</x-app-layout>