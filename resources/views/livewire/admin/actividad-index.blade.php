
    <div class="card">
        <div class="card-header">
            <input wire:model="search" class="form-control" placeholder="Ingrese el nombre de la actividad">
        </div>
        @can('admin.actividads.create', Model::class)
        <div class="card-header" >
            <a class="btn btn-primary" href="{{route("admin.actividads.create")}}">Nueva Actividad </a>
        </div>
            
        @endcan
        @if ($actividads->count())
            
        
        <div class="card-body">
           
            <table class="table table-stripe">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Actividad</th>
                        <th>Horas</th>
                        <th>Fecha</th>
                        <th>Opciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($actividads as $actividad)
                    <tr>
                        <td>{{$actividad->id}}</td>
                        <td>{{$actividad->nombre}}</td>
                        <td>{{$actividad->horas}}</td>
                        <td>{{$actividad->fecha}}</td>
                        <td width="10px" >
                            @can('admin.actividads.edit')
                            <a class="btn btn-primary" href="{{route('admin.actividads.edit',$actividad)}}">Editar</a>
                            @endcan
                            
                        </td>

                    </tr>
                        
                    @endforeach

                </tbody>
            </table>
        </div>
        <div class="card-footer">
            {{$actividads->links()}}
        </div>
            
        @else
        <div class="card-body">
            <strong>No hay registros</strong>
        </div>
        @endif
    </div>