
    <div class="card">
        <div class="card-header">
            <input wire:model="search" class="form-control" placeholder="Ingrese nombre de la actividad">
        </div>
        @if ($actividads->count())
            
        
        <div class="card-body">
           
            <table class="table table-stripe">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Actividad</th>
                        <th>Horas</th>
                        <th>Fecha</th>
                        <th style="width: 40%" colspan="3">Opciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($actividads as $actividad)
                    <tr>
                        <td>{{$actividad->id}}</td>
                        <td>{{$actividad->nombre}}</td>
                        <td>{{$actividad->horas}}</td>
                        <td>{{$actividad->fecha}}</td>
                        <td  >
                            @can('admin.codes.create')
                            <a class="btn btn-primary" href="{{route('admin.codes.edit',$actividad->id)}}">Generar Códigos</a>
                            @endcan
                            
                        </td>
                        <td  >
                            @can('admin.codes.create')
                            <a class="btn btn-success" href="{{route('admin.codes.show',$actividad->id)}}">Ver Códigos</a>
                            @endcan
                            
                        </td>
                        <td  >
                            @can('admin.codes.create')
                            <a class="btn btn-info" href="{{route('admin.validars.edit',$actividad->id)}}">Validar</a>
                            @endcan
                            
                        </td>

                    </tr>
                        
                    @endforeach

                </tbody>
            </table>
        </div>
        <div class="card-footer">
            {{$actividads->links()}}
        </div>
            
        @else
        <div class="card-body">
            <strong>No hay registros</strong>
        </div>
        @endif
    </div>