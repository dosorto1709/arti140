
    <div class="card">
        <div class="card-header">
            <input wire:model="search" class="form-control" placeholder="Ingrese el número de cuenta del estudiante">
        </div>
        @if ($estudiantes->count())
            
        
        <div class="card-body">
           
            <table class="table table-stripe">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>nombre</th>
                        <th>Cuenta</th>
                        <th>Correo</th>
                        <th>Telefono</th>
                        <th>Opciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($estudiantes as $estudiante)
                    <tr>
                        <td>{{$estudiante->id}}</td>
                        <td>{{$estudiante->nombre.' '.$estudiante->apellido}}</td>
                        <td>{{$estudiante->cuenta}}</td>
                        <td>{{$estudiante->correo}}</td>
                        <td>{{$estudiante->telefono}}</td>
                        <td width="10px" >
                            @can('admin.estudiantes.edit')
                            <a class="btn btn-primary" href="{{route('admin.estudiantes.edit',$estudiante)}}">Editar</a>
                            @endcan
                            
                        </td>

                    </tr>
                        
                    @endforeach

                </tbody>
            </table>
        </div>
        <div class="card-footer">
            {{$estudiantes->links()}}
        </div>
            
        @else
        <div class="card-body">
            <strong>No hay registros</strong>
        </div>
        @endif
    </div>