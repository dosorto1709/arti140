<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Facultad extends Model
{
    use HasFactory;
    protected $fillable=['nombre','centro_id','logo'];

    public function users(){
        return $this->hasMany(User::class);
    }
    public function estudiantes(){
        return $this->hasMany(Estudiante::class);
    }

    public function carreras(){
        return $this->hasMany(Carrera::class);
    }
    public function centros(){
        return $this->belongsTo(Centro::class);
    }
    public function getRouteKeyName()
    {
        return 'nombre';
    }
}
