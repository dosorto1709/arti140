<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Constancia extends Model
{
    protected $fillable=[
        'file',
        'codigo',
        'user_id',
    ];
    
    
    public function getRouteKeyName()
    {
        return 'codigo';
    }
    use HasFactory;
}
