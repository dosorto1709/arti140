<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Centro extends Model
{
    use HasFactory;
    protected $fillable=['nombre',
    'ciudad',
    'depto',
    'logo',
    'direccion',
    'url',
    'voae',
];

    public function facultads(){
        return $this->hasMany(Facultad::class);
    }
    public function users(){
        return $this->hasMany(User::class);
    }
    public function estudiantes(){
        return $this->hasMany(Estudiante::class);
    }

    public function getRouteKeyName()
    {
        return 'nombre';
    }
}
