<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Carrera extends Model
{
    use HasFactory;
    protected $fillable=[
        'nombre',
        'facultad_id',
        'centro_id',
        'telefono',
        'ext',
        'cordinador',
        'logo',
    ];

   
    public function centros(){
        return $this->belongsTo(Centro::class);
    }
    public function facultads(){
        return $this->belongsTo(Facultad::class);
    }
    public function actividads(){
        return $this->hasMany(Actividad::class);
    }
    public function Estudiantes(){
        return $this->hasMany(Estudiante::class);
    }
}
