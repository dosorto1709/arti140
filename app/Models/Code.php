<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Code extends Model
{
    public function actividads(){
        return $this->belongsTo(Actividad::class);
    }
    use HasFactory;
}
