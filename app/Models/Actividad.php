<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Actividad extends Model
{
    use HasFactory;

    protected $fillable=[
        'nombre',
        'carrera_id',
        'centro_id',
        'facultad_id',
        'dimension',
        'horas',
        'fecha',
    ];

    protected $dates = [
        'fecha', 'mi_hora'
    ];

    protected $casts = [
        'fecha' => 'datetime:Y-m-d',
        //'mi_hora' => 'datetime:H:i:s'
    ];

    public function codes(){
        return $this->hasMany(Code::class);
    }
    /*public function estudiantes(){
        return $this->belongsToMany(Estudiante::class);
    }*/
    public function carreras(){
        return $this->belongsTo(Carrera::class);
    }
}
