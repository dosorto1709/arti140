<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Estudiante extends Model
{
    use HasFactory;
    protected $fillable=['nombre',
    'apellido',
    'cuenta',
    'telefono',
    'correo',
    'carrera_id'
];
    /*public function actividads(){
        return $this->belongsToMany(Actividad::class);
    }*/
    public function carreras(){
        return $this->belongsTo(Carrera::class);
    }
    public function centros(){
        return $this->belongsTo(Centro::class);
    }
    public function facultads(){
        return $this->belongsTo(Facultad::class);
    }
}
