<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Acumulado;
use App\Models\Carrera;
use App\Models\Constancia;
use App\Models\Estudiante;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\Storage;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class ConstanciaController extends Controller
{


    public function __construct()
    {
        $this->middleware('can:admin.constancias.index')->only('index');
        $this->middleware('can:admin.constancias.show')->only('show');
        $this->middleware('can:admin.constancias.edit')->only('edit');

    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("admin.constancias.index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function generar()
    {
        $chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $res = "";
        for ($i = 0; $i < 8; $i++) {
            $res .= $chars[mt_Rand(0, strlen($chars) - 1)];
        }
        return $res;
    }
    public function show($id)
    {
        $estudiante = Estudiante::join("facultads", "facultads.id", "=", "estudiantes.facultad_id")
            ->join("centros", "centros.id", "=", "estudiantes.centro_id")
            ->join("carreras", "carreras.id", "=", "estudiantes.carrera_id")
            ->where('estudiantes.id', $id)
            ->select(
                "centros.nombre as centro",
                "facultads.nombre as facultad",
                "carreras.nombre as carrera",
                "carreras.cordinador",
                "carreras.telefono as carreraTelefono",
                "carreras.ext as carreraExt",
                "centros.logo as logoCentro",
                "centros.ciudad",
                "centros.direccion",
                "centros.url",
                "centros.voae",
                "centros.depto",
                "facultads.logo as logoFacultad",
                "carreras.logo as logoCarrera",
                'estudiantes.foto',
                'estudiantes.telefono',
                'estudiantes.nombre',
                'estudiantes.cuenta',
                'estudiantes.apellido',
                'estudiantes.correo',
                'estudiantes.id',
            )
            ->first();
        $actividades = Acumulado::where('estudiante_id', $id)
            ->where('estado', 'Acreditado')
            ->join("actividads", "actividads.id", "=", "acumulados.actividad_id")
            ->join("codes", "codes.id", "=", "acumulados.code_id")
            ->select(
                "actividads.nombre",
                "actividads.dimension",
                "actividads.id",
                "acumulados.estado",
                "actividads.fecha",
                "actividads.horas",
                'codes.codigo',
            )
            ->get();
        //$actividades= $estudiante->actividads()->get('');

        //return $actividades;
        $suma = 0;

        foreach ($actividades as $actividad) {
            if ($actividad->estado == 'Acreditado') {
                $suma = $suma + $actividad->horas;
            }
        }
        $estudiante->actividades = $actividades;
        $estudiante->suma = $suma;
        $codigo = $this->generar();
        $estudiante->codigo = $codigo;
        $estudiante->logoCentro = Storage::url($estudiante->logoCentro);
        $estudiante->logoFacultad = Storage::url($estudiante->logoFacultad);
        $estudiante->logoCarrera = Storage::url($estudiante->logoCarrera);
        $estudiante->foto = Storage::url($estudiante->foto);

        $estudiante->anioEduc = '"' . env('APP_ANIO') . '"'; //APP_URL
        $estudiante->proyecto = '"' . env('APP_PROYECTO') . '"'; //'"La Educación es la Primera Necesidad de La Republica"';

        $cuenta =  (int)substr($estudiante->cuenta, 0, -7);
        //return $cuenta;
        if ($cuenta >= 2015) {
            $estudiante->horas = 60;
            $estudiante->horaT = 'SESENTA';
        } else {
            $estudiante->horas = 40;
            $estudiante->horaT = 'CUARENTA';
        }

        $user = auth()->user();

        $constancia = [
            'file' => 'public/constancias/' . 'CONSTANCIA-ARTI140-' . $estudiante->cuenta . '.pdf',
            'user_id' => $user->id
        ];
        $constancia = Constancia::create($constancia);
        $constancia->fresh();
        $constancia = Constancia::where('id', $constancia->id)->first();
        //return $constancia;
        $qrcode = base64_encode(QrCode::format('svg')->size(80)->errorCorrection('H')->generate(env('APP_URL') . '/documento?id=' . $constancia->codigo));
        $estudiante->qrcode = $qrcode;
        $estudiante->codigo = $constancia->codigo;
        $pdf = PDF::loadView('admin.constancias.show', $estudiante);
        Storage::put('public/constancias/' . 'CONSTANCIA-ARTI140-' . $estudiante->cuenta . '.pdf', $pdf->output());




        $pdf->getDomPDF()->setHttpContext(
            stream_context_create([
                'ssl' => [
                    'allow_self_signed' => TRUE,
                    'verify_peer' => FALSE,
                    'verify_peer_name' => FALSE,
                ]
            ])
        );

        return $pdf->download('CONSTANCIA-ARTI140-' . $estudiante->cuenta . '.pdf');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $carreras = Carrera::pluck('nombre', 'id');
        //$estudiante = Estudiante::where('id',$id)->first();
        $estudiante = Estudiante::join("facultads", "facultads.id", "=", "estudiantes.facultad_id")
            ->join("centros", "centros.id", "=", "estudiantes.centro_id")
            ->join("carreras", "carreras.id", "=", "estudiantes.carrera_id")
            ->where('estudiantes.id', $id)
            ->select(
                "centros.nombre as centro",
                "facultads.nombre as facultad",
                "carreras.nombre as carrera",
                "centros.logo as logoCentro",
                "facultads.logo as logoFacultad",
                "carreras.logo as logoCarrera",
                'estudiantes.foto',
                'estudiantes.telefono',
                'estudiantes.nombre',
                'estudiantes.cuenta',
                'estudiantes.apellido',
                'estudiantes.correo',
                'estudiantes.id',
            )
            ->first();

        $actividades = Acumulado::where('estudiante_id', $estudiante->id)
            ->join("actividads", "actividads.id", "=", "acumulados.actividad_id")
            ->join("codes", "codes.id", "=", "acumulados.code_id")
            ->select(
                "actividads.nombre",
                "actividads.dimension",
                "actividads.id",
                "acumulados.estado",
                "actividads.fecha",
                "actividads.horas",
                'codes.codigo',
            )
            ->get();
        $suma = 0;

        foreach ($actividades as $actividad) {
            if ($actividad->estado == 'Acreditado') {
                $suma = $suma + $actividad->horas;
            }
        }
        return view("admin.constancias.edit", compact("estudiante", 'actividades', 'suma'));
        //return $id;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
