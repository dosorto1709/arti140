<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Carrera;
use App\Models\Centro;
use App\Models\Facultad;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:admin.users.index')->only('index');
        $this->middleware('can:admin.users.edit')->only('edit', 'update');
    }
    public function index()
    {
        return view("admin.users.index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $roles = Role::all();
        $centros = Centro::pluck('nombre', 'id');
        $facultads = Facultad::join("centros", "centros.id", "=", "facultads.centro_id")
            ->select('facultads.id', DB::raw('CONCAT(facultads.nombre, "--", centros.nombre) AS nombre'))
            ->get()
            ->pluck('nombre', 'id');
        $carreras = Carrera::join("centros", "centros.id", "=", "carreras.centro_id")
            ->join('facultads', 'facultads.id', '=', 'carreras.facultad_id')
            ->select('carreras.id', DB::raw('CONCAT(carreras.nombre, " --",facultads.nombre, "--", centros.nombre) AS nombre'))
            ->get()
            ->pluck('nombre', 'id');
        //$carreras = Carrera::pluck('nombre','id');
        return view("admin.users.edit", compact('user', 'roles', 'centros', 'facultads', 'carreras'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $user->update($request->all());
        $user->roles()->sync($request->roles);
        return redirect()->route('admin.users.edit', $user)->with('info', 'Se asignó los roles correctamente');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }
}
