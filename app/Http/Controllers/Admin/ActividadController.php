<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Actividad;
use Illuminate\Http\Request;

class ActividadController extends Controller
{

    public function __construct()
    {
        $this->middleware('can:admin.actividads.index')->only('index');
        $this->middleware('can:admin.actividads.destroy')->only('destroy');
        $this->middleware('can:admin.actividads.create')->only('create', 'store');
        $this->middleware('can:admin.actividads.edit')->only('edit', 'update');
    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("admin.actividads.index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $dimensions = [
            'Social' => 'Social',
            'Cultural-Artístico' => 'Cultural-Artístico',
            'Deportiva' => 'Deportiva',
            'Científico-Académico' => 'Científico-Académico'
        ];

        return view("admin.actividads.create", compact('dimensions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nombre' => 'required',
            'horas' => 'required',
            'dimension' => 'required',
            'fecha' => 'required',
        ]);

        $actividad = new Actividad;
        $actividad->nombre = $request->get('nombre');
        $actividad->horas = $request->get('horas');
        $actividad->fecha = $request->get('fecha');
        $actividad->dimension = $request->get('dimension');
        $actividad->centro_id = auth()->user()->centro_id;
        $actividad->facultad_id = auth()->user()->facultad_id;
        $actividad->carrera_id = auth()->user()->carrera_id;
        $actividad->save();

        // redireccionar a la vista de la actividad
        return redirect()->route('admin.actividads.edit', $actividad)
            ->with('info', 'La Actividad se almacenó con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Actividad $actividad)
    {
        $dimensions = [
            'Social' => 'Social',
            'Cultural-Artístico' => 'Cultural-Artístico',
            'Deportiva' => 'Deportiva',
            'Científico-Académico' => 'Científico-Académico'
        ];

        return view("admin.actividads.edit", compact('actividad', 'dimensions'));
    }

    public function update(Request $request, Actividad $actividad)
    {
        $request->validate([
            'nombre' => 'required',
            'horas' => 'required',
            'dimension' => 'required',
            'fecha' => 'required',
        ]);

        $actividad->update([
            'nombre' => $request->get('nombre'),
            'horas' => $request->get('horas'),
            'fecha' => $request->get('fecha'),
            'dimension' => $request->get('dimension'),
            'centro_id' => auth()->user()->centro_id,
            'facultad_id' => auth()->user()->facultad_id,
            'carrera_id' => auth()->user()->carrera_id,
        ]);
        return redirect()->route('admin.actividads.edit', $actividad)->with('info', 'La Actividad se Actualizó con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
