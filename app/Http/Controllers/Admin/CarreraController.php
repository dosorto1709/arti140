<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Carrera;
use App\Models\Centro;
use App\Models\Facultad;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class CarreraController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:admin.carreras.index')->only('index');
        $this->middleware('can:admin.carreras.create')->only('create', 'store');
        $this->middleware('can:admin.carreras.edit')->only('edit', 'update');
        $this->middleware('can:admin.carreras.destroy')->only('destroy');
    }
    public function index()
    {
        //$facultads = Facultad::all();
        $carreras = Carrera::join("facultads", "facultads.id", "=", "carreras.facultad_id")
            ->join("centros", "centros.id", "=", "facultads.centro_id")
            ->select("centros.nombre as centro", "facultads.nombre as facultad", "carreras.nombre", "carreras.id")
            ->get();
        //return $carreras;
        return view("admin.carreras.index", compact("carreras"));
    }


    public function create()
    {
        $centros = Centro::pluck('nombre', 'id');
        //$facultads = Facultad::pluck('nombre','id');
        $facultads = Facultad::join("centros", "centros.id", "=", "facultads.centro_id")
            ->select('facultads.id', DB::raw('CONCAT(facultads.nombre, "--", centros.nombre) AS nombre'))
            //->select('CONCAT(facultads.nombre, ", ", centros.nombre) AS nombre',"facultads.id" )
            ->get()
            ->pluck('nombre', 'id');
        //return $facultads;
        return view("admin.carreras.create", compact('centros', 'facultads'));
    }


    public function store(Request $request)
    {
        //return $request->file('logo');

        $request->validate([
            'nombre' => 'required|unique:carreras',
            'centro_id' => 'required',
            'facultad_id' => 'required',
            'logo' => 'required',
            'telefono' => 'required',
            'ext' => 'required',
            'cordinador' => 'required'

        ]);

        $url =  request()->file('logo')->store('public/carreras');
        $carrera = new Carrera;
        $carrera->nombre = $request->get('nombre');
        $carrera->logo = $url;
        $carrera->facultad_id = $request->get('facultad_id');
        $carrera->centro_id = $request->get('centro_id');
        $carrera->telefono = $request->get('telefono');
        $carrera->ext = $request->get('ext');
        $carrera->cordinador = $request->get('cordinador');
        $carrera->save();
        return redirect()->route('admin.carreras.edit', $carrera)->with('info', 'La Carrera se almacenó con exito');
    }



    public function edit(Carrera $carrera)
    {
        $centros = Centro::pluck('nombre', 'id');
        //$facultads = Facultad::pluck('nombre','id');
        $facultads = Facultad::join("centros", "centros.id", "=", "facultads.centro_id")
            ->select('facultads.id', DB::raw('CONCAT(facultads.nombre, "--", centros.nombre) AS nombre'))
            //->select('CONCAT(facultads.nombre, ", ", centros.nombre) AS nombre',"facultads.id" )
            ->get()
            ->pluck('nombre', 'id');
        return view("admin.carreras.edit", compact("carrera", 'facultads', 'centros'));
    }


    public function update(Request $request, Carrera $carrera)
    {
        $request->validate([
            'nombre' => "required|unique:carreras,nombre,$carrera->id",
            'facultad_id' => 'required',
            'telefono' => 'required',
            'ext' => 'required',
            'cordinador' => 'required',
            'logo' => 'required',


        ]);

        if ($request->file('logo')) {
            $url =  request()->file('logo')->store('public/centros');
            $carrera->update([
                'logo' => $url,
                'nombre' => $request->get('nombre'),
                'facultad_id' => $request->get('facultad_id'),
                'centro_id' => $request->get('centro_id'),
                'telefono' => $request->get('telefono'),
                'ext' => $request->get('ext'),
                'cordinador' => $request->get('cordinador')
            ]);
        } else {
            $carrera->update($request->all());
        }


        return redirect()->route('admin.carreras.edit', $carrera)->with('info', 'La Carrera se actualizó con exito');
    }

    public function destroy(Carrera $carrera)
    {
        $carrera->delete();
        return redirect()->route('admin.carreras.index', $carrera)->with('info', 'La carrera se eliminó con exito');
    }
}
