<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Actividad;
use App\Models\Acumulado;
use App\Models\Code;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;

class AcumuladoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('can:admin.validars.index')->only('index');
        $this->middleware('can:admin.validars.create')->only('show');
        $this->middleware('can:admin.validars.edit')->only('edit', 'update');
        $this->middleware('can:admin.validars.destroy')->only('destroy');
    }
    public function index(Request $request)
    {
        $codigos = Code::where('actividad_id', $request->id)
            ->join('actividads', 'actividads.id', 'codes.actividad_id')
            ->where('usado', 0)->get();
        //return $codigos;
        return view("admin.codes.print", compact("codigos",));
        //return $pdf->download('PRINT-CODES-'.$request->id.'.pdf');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = auth()->user();
        $acumulado = Acumulado::find($id);
        
        $acumulado->estado = 'Acreditado';
        $acumulado->validado_user_id = $user->id;
        $acumulado->save();


        return redirect()->route('admin.validars.edit', $acumulado->actividad_id)
            ->with('info', 'Cambio realizado con éxito');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $actividad = Actividad::where('id', $id)->first();

        $codes = Acumulado::join("codes", "codes.id", "=", "acumulados.code_id")
            ->join("estudiantes", "estudiantes.id", "=", "acumulados.estudiante_id")
            ->join("carreras", "carreras.id", "=", "estudiantes.carrera_id")
            ->where('acumulados.actividad_id', $id)
            ->select(
                "acumulados.id",
                "acumulados.estado",
                "estudiantes.nombre",
                "estudiantes.apellido",
                "estudiantes.cuenta",
                "carreras.nombre as carrera",
                "codes.codigo",
            )
            ->latest('id')->get();
        //return $codes;
        return view("admin.codes.update", compact("actividad", 'id', 'codes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //return $id;
        $user = auth()->user();
        //$acumulado = Acumulado::where($id);
        $acumulado = Acumulado::where('actividad_id', $id)
            ->where('estado', 'Pendiente')

            ->update([
                'estado' => 'Acreditado',
                'validado_user_id' => $user->id
            ]);

        return redirect()->route(
            'admin.validars.edit',
            $id
        )->with('info', 'Cambio realizado con éxito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //'Pendiente', 'Acreditado','No Asignado'
        $user = auth()->user();

        $acumulado = Acumulado::find($id);
        $acumulado->estado = 'No Asignado';
        $acumulado->validado_user_id = $user->id;
        $acumulado->save();


        return redirect()->route('admin.validars.edit', $acumulado->actividad_id)
            ->with('info', 'Cambio realizado con éxito');
    }
}
