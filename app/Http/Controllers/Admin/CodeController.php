<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Actividad;
use App\Models\Acumulado;
use App\Models\Code;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use PhpParser\Node\Stmt\Return_;

class CodeController extends Controller
{
    public function __construct()
    {
        $this->middleware('can:admin.codes.index')->only('index');
        $this->middleware('can:admin.codes.show')->only('show');
        $this->middleware('can:admin.codes.update')->only('edit');
        $this->middleware('can:admin.codes.destroy')->only('destroy');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("admin.codes.index");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function generar()
    {
        $chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $res = "";
        for ($i = 0; $i < 8; $i++) {
            $res .= $chars[mt_Rand(0, strlen($chars) - 1)];
        }
        return $res;
    }
    public function store(Request $request)
    {

        //return $request;
        if ($request->infinito) {
            $code = new Code;
            $code->infinito = true;
            $code->codigo = $this->generar();
            $code->actividad_id = $request->actividad_id;
            $code->save();
        } else {
            $request->validate([
                'cantidad' => 'required',
            ]);
            for ($i = 1; $i <= $request->cantidad; $i++) {
                $code = new Code;
                $code->infinito = false;
                $code->codigo = $this->generar();
                $code->actividad_id = $request->actividad_id;
                $code->save();
            }
        }
        $id = $request->actividad_id;

        return redirect()->route(
            'admin.codes.show',
            $id
        )->with('info', 'Códigos generados Exitosamente!!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $actividad = Actividad::where('id', $id)
            ->first();
        $codes = Code::where('actividad_id', $id)
            ->latest('id')
            ->get();
        //return $actividad;
        return view("admin.codes.show", compact("actividad", 'id', 'codes'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $actividad = Actividad::where('id', $id)->first();
        //return $actividad;
        return view("admin.codes.edit", compact("actividad"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Code $code)
    {
        //return $code; 
        Code::where('id', $code->id)->update([
            'usado' => true,
        ]);
        //$actividad = Actividad::where('id',$code->actividad_id)->first();
        //return $actividad;
        return redirect()->route('admin.codes.show', $code->actividad_id)
            ->with('info', 'El código: ' . $code->codigo . ' ha sido desactivado');
    }
}
