<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Centro;
use Illuminate\Support\Facades\Storage;

class CentroController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('can:admin.centros.index')->only('index');
        $this->middleware('can:admin.centros.create')->only('create','store');
        $this->middleware('can:admin.centros.edit')->only('edit','update');
        $this->middleware('can:admin.centros.destroy')->only('destroy');
    }

    public function index()
    {
        $centros = Centro::all();
        return view("admin.centros.index", compact("centros"));
    }

    public function create()
    {
        
        return view("admin.centros.create");
    }

    public function store(Request $request)
    {
        $request->validate([
            'nombre'=>'required|unique:centros',
        ]);
        //$url = Storage::put('images', $request->file('logo'));
        $url =  request()->file('logo')->store('public/centros'); 
        $centro = new Centro;
        $centro->nombre = $request->get('nombre');
        $centro->logo = $url;
        $centro->depto= $request->get('depto');
        $centro->ciudad= $request->get('ciudad');
        $centro->voae= $request->get('voae');
        $centro->direccion= $request->get('direccion');
        $centro->url= $request->get('url');
        $centro->save();
        //$centro = Centro::create($centro->all());
        return redirect()->route('admin.centros.edit',$centro)->with('info','El centro se almacenó con exito');
    }

 
    public function edit(Centro $centro)
    {
        return view("admin.centros.edit",compact("centro"));
    }

    public function update(Request $request,Centro $centro)
    {
        $request->validate([
            'nombre'=>"required|unique:centros,nombre,$centro->id",
        ]);
        if($request->file('logo')){
            $url =  request()->file('logo')->store('public/centros');
            $centro->update([
                'logo'=>$url,
                'nombre'=> $request->get('nombre'),
                'depto'=> $request->get('depto'),
                'ciudad'=> $request->get('ciudad'),
                'voae'=> $request->get('voae'),
                'direccion'=> $request->get('direccion'),
                'url'=> $request->get('url'),
            ]); 

        }else{
            $centro->update($request->all());

        } 
        
        return redirect()->route('admin.centros.edit',$centro)->with('info','El centro se actualizó con exito');
    }

    public function destroy(Centro $centro)
    {
        $centro->delete();
        return redirect()->route('admin.centros.index',$centro)->with('info','El centro se eliminó con exito');

    }
}
