<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Centro;
use Illuminate\Http\Request;
use App\Models\Facultad;
class FacultadController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('can:admin.facultads.index')->only('index');
        $this->middleware('can:admin.facultads.create')->only('create','store');
        $this->middleware('can:admin.facultads.edit')->only('edit','update');
        $this->middleware('can:admin.facultads.destroy')->only('destroy');
    }

    public function index()
    {
        //$facultads = Facultad::all();
        $facultads = Facultad::join("centros", "centros.id", "=", "facultads.centro_id")
        ->select("centros.nombre as centro","facultads.nombre","facultads.id" )
        ->get();
        //return $facultads;
        return view("admin.facultads.index", compact("facultads"));
    }

    public function create()
    {
        $centros = Centro::pluck('nombre','id');
        return view("admin.facultads.create", compact('centros'));
    }


    public function store(Request $request)
    {

    
        $request->validate([
            'nombre'=>'required|unique:facultads',
            'centro_id'=>'required',
            'logo'=>'required'
        ]);
        $url =  request()->file('logo')->store('public/facultads'); 
        $facultad = new Facultad;
        $facultad->nombre = $request->get('nombre');
        $facultad->logo = $url;
        $facultad->centro_id= $request->get('centro_id');
        $facultad->save();
        //$centro = Centro::create($centro->all());
        return redirect()->route('admin.facultads.edit',$facultad)->with('info','La Facultad se almacenó con exito');
        
        //$facultad = Facultad::create($request->all());
        
    }


    public function edit(Facultad $facultad)
    {
        $centros = Centro::pluck('nombre','id');
        return view("admin.facultads.edit", compact("facultad",'centros'));
    }

    public function update(Request $request, Facultad $facultad)
    {
        $request->validate([
            'nombre'=>"required|unique:facultads,nombre,$facultad->id",
            'centro_id'=>'required'
            
        ]);
        if($request->file('logo')){
            $url =  request()->file('logo')->store('public/facultads');
            $facultad->update([
                'logo'=>$url,
                'nombre'=> $request->get('nombre'),
                'centro_id'=> $request->get('centro_id'),
            ]); 

        }else{
            $facultad->update($request->all());

        } 
        return redirect()->route('admin.facultads.edit',$facultad)->with('info','La Facultad se actualizó con exito');
    }


    public function destroy(Facultad $facultad)
    {
        $facultad->delete();
        return redirect()->route('admin.facultads.index',$facultad)->with('info','La facultad se eliminó con exito');
    }
}
