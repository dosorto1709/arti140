<?php

namespace App\Http\Controllers\Estudiante;

use App\Http\Controllers\Controller;
use App\Models\Actividad;
use App\Models\Acumulado;
use App\Models\Carrera;
use App\Models\Centro;
use App\Models\Code;
use App\Models\Estudiante;
use App\Models\Facultad;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Livewire\WithPagination;

class ConsultaController extends Controller
{
    public $cent, $fac;

    public function index(Request $request)
    {
        $request->validate([
            'cuenta' => 'required'

        ]);

        $cuenta = $request->cuenta;
        //Estudiante::where('cuenta',$cuenta);
        if (Estudiante::where('cuenta', $request->cuenta)->exists()) {
            $estudiante = Estudiante::join("facultads", "facultads.id", "=", "estudiantes.facultad_id")
                ->join("centros", "centros.id", "=", "estudiantes.centro_id")
                ->join("carreras", "carreras.id", "=", "estudiantes.carrera_id")
                ->where('cuenta', $request->cuenta)
                ->select(
                    "centros.nombre as centro",
                    "facultads.nombre as facultad",
                    "carreras.nombre as carrera",
                    "centros.logo as logoCentro",
                    "facultads.logo as logoFacultad",
                    "carreras.logo as logoCarrera",
                    'estudiantes.foto',
                    'estudiantes.nombre',
                    'estudiantes.cuenta',
                    'estudiantes.apellido',
                    'estudiantes.correo',
                    'estudiantes.id',
                )
                ->first();
            //return $estudiante;
            //$actividades = Actividad::all();
            $actividades = Acumulado::where('estudiante_id', $estudiante->id)
                ->join("actividads", "actividads.id", "=", "acumulados.actividad_id")
                ->join("codes", "codes.id", "=", "acumulados.code_id")
                ->select(
                    "actividads.nombre",
                    "actividads.dimension",
                    "actividads.id",
                    "acumulados.estado",
                    "actividads.fecha",
                    "actividads.horas",
                    'codes.codigo',
                )
                ->get();
            //$actividades= $estudiante->actividads()->get('');

            //return $actividades;
            $suma = 0;

            foreach ($actividades as $actividad) {
                if ($actividad->estado == 'Acreditado') {
                    $suma = $suma + $actividad->horas;
                }
            }

            return view('estudiante.consultas.index', compact('estudiante', 'actividades', 'suma'));
        } else {
            $centros = Centro::pluck('nombre', 'id');
            $facultads = Facultad::pluck('nombre', 'id');
            $carreras = Carrera::pluck('nombre', 'id');
            return view('estudiante.consultas.create', compact('centros', 'facultads', 'carreras', 'cuenta'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'cuenta' => 'required',
            'nombre' => 'required',
            'apellido' => 'required',
            'telefono' => 'required',
            'correo' => 'required',
            'centro_id' => 'required',
            'facultad_id' => 'required',
            'carrera_id' => 'required',
            'foto' => 'required',


        ]);
        //return $request;
        $url =  request()->file('foto')->store('public/estudiantes');
        $estudiante = new Estudiante;
        $estudiante->cuenta = $request->get('cuenta');
        $estudiante->foto = $url;
        $estudiante->facultad_id = $request->get('facultad_id');
        $estudiante->centro_id = $request->get('centro_id');
        $estudiante->carrera_id = $request->get('carrera_id');
        $estudiante->nombre = $request->get('nombre');
        $estudiante->apellido = $request->get('apellido');
        $estudiante->telefono = $request->get('telefono');
        $estudiante->correo = $request->get('correo');
        $estudiante->save();
        $cuenta = $request->get('cuenta');
        //return view('estudiante.consultas.show', compact('estudiante'));
        return redirect()->route('consulta', $cuenta)->with('info', 'Estudiante se almacenó con exito');

        //$cuenta=  $request->cuenta;
        //$estudiante= Estudiante::where('cuenta',$request->cuenta);
        //$estudiante = Estudiante::where('carrera_id',auth()->user()->carrera_id)
        //                        ->where('cuenta',$cuenta);
        //return view('estudiante.consultas.show', compact('estudiante'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($cuenta)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //$estudiante = Estudiante::where('id', $request->id)->first();
        //$cuenta = $estudiante->cuenta;
        if (Code::where('codigo', $request->codigo)->exists()) {
            if (Code::where('codigo', $request->codigo)->where('usado', false)->exists()) {
                $codigo = Code::where('codigo', $request->codigo)->where('usado', false)->first();
                if (Acumulado::where('actividad_id', $codigo->actividad_id)->where('estudiante_id', $request->id)->exists() == false) {
                    $acumulado = new Acumulado;
                    $acumulado->code_id = $codigo->id;
                    $acumulado->estudiante_id = $request->get('id');
                    $acumulado->actividad_id = $codigo->actividad_id;
                    $acumulado->save();
                    if ($codigo->infinito == false) {
                        Code::where('id', $codigo->id)->update([
                            'usado' => true,
                        ]);
                        /*$codigo->update([
                        'usado'=>true,
                    ]);*/
                    }
                    //$detail = "Codigo existe y no ha sido usado";
                    return Redirect::back()->with('info', 'Actividad registrada');
                } else {

                    return Redirect::back()->with('info', 'No puede registrar la misma actividad más de una vez');
                }
            } else
                return Redirect::back()->with('info', 'ERROR: El codigó ya había sido tomado ');
        } else {
            return Redirect::back()->with('info', 'ERROR: Código no existe');
        }
        //return redirect()->route('consulta',$cuenta)->with('info','ERROR: Código no existe');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
