<?php

namespace App\Http\Livewire\Admin;

use Livewire\Component;
use Livewire\WithPagination;
use App\Models\Actividad;

class CodeIndex extends Component
{
    use WithPagination;
    protected $paginationTheme="bootstrap";
    public $search;

    public function updatingSearch(){
        $this->resetPage();
    }


    public function render()
    {
        $actividads = Actividad::where('carrera_id',auth()->user()->carrera_id)
                                ->where('nombre','LIKE','%'.$this->search.'%')
                                //->orwhere('cuenta','LIKE','%'.$this->search.'%') 
                                ->latest('id')
                                ->paginate();
        return view('livewire.admin.code-index',compact('actividads'));
    }
}
