<?php

namespace App\Http\Livewire\Admin;

use App\Models\Estudiante;
use Livewire\Component;
use Livewire\WithPagination;


class ConstanciaIndex extends Component
{
    use WithPagination;
    protected $paginationTheme="bootstrap";
    public $search;

    public function updatingSearch(){
        $this->resetPage();
    }
    public function render()
    {
        $carrera= auth()->user();
        //return $carrera_id;
        $estudiantes = Estudiante::where('carrera_id',auth()->user()->carrera_id)
                                ->where('cuenta','LIKE','%'.$this->search.'%')
                                //->orwhere('cuenta','LIKE','%'.$this->search.'%') 
                                ->latest('id')
                                ->paginate();
        return view('livewire.admin.constancia-index',compact('estudiantes','carrera'));
    }
}
