<?php

use App\Http\Controllers\Admin\DocumentoController;
use App\Http\Controllers\Estudiante\ConsultaController;
use App\Models\Estudiante;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Input;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('consulta', [ConsultaController::class, 'index'])->name('consulta');
Route::post('registro', [ConsultaController::class, 'store'])->name('registro');
Route::post('acumular', [ConsultaController::class, 'update'])->name('acumular');

//Route::get('documento', [DocumentoController::class, 'index'])->name('documento');

/*
Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');
*/