<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Estudiante\ConsultaController;


Route::resource('consultas', ConsultaController::class)
    ->only(['index','store','update'])
    ->names('estudiante.consultas');
