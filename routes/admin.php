<?php


use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\Admin\CentroController;
use App\Http\Controllers\Admin\FacultadController;
use App\Http\Controllers\Admin\CarreraController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\RoleController;
use App\Http\Controllers\Admin\EstudianteController;
use App\Http\Controllers\Admin\ActividadController;
use App\Http\Controllers\Admin\AcumuladoController;
use App\Http\Controllers\Admin\CodeController;
use App\Http\Controllers\Admin\ConstanciaController;
use App\Http\Controllers\Admin\DocumentoController;
use App\Http\Controllers\Estudiante\ConsultaController;

Route::get('', [HomeController::class, 'index'])->name("admin.home")
    ->middleware('can:admin.home');

// middlewares completos
Route::resource('centros', CentroController::class)
    ->only(['index', 'create', 'store', 'edit', 'update', 'destroy'])
    ->names("admin.centros");

// middlewares completos
Route::resource('facultads', FacultadController::class)
    ->only(['index', 'create', 'store', 'edit', 'update', 'destroy'])
    ->names("admin.facultads");

// middlewares completos
Route::resource('carreras', CarreraController::class)
    ->only(['index', 'create', 'store', 'edit', 'update', 'destroy'])
    ->names("admin.carreras");

// middlewares completos
Route::resource('users', UserController::class)
    ->only(['index', 'edit', 'update'])
    ->names("admin.users");

// middlewares completos
Route::resource('roles', RoleController::class)
    ->only(['index', 'create', 'store', 'edit', 'update', 'destroy'])
    ->names('admin.roles');

//  Middlewares completos
Route::resource('estudiantes', EstudianteController::class)
    ->only(['index', 'edit', 'update'])
    ->names('admin.estudiantes');

// Middlewares completos
Route::resource('actividads', ActividadController::class)
    ->only(['index', 'create', 'store', 'edit', 'update'])
    ->names('admin.actividads');


// Middlewares completos
Route::resource('codes', CodeController::class)
    ->only(['index', 'store','show','edit','destroy'])
    ->names('admin.codes');


// Middlewares completos
Route::resource('constancias', ConstanciaController::class)
    ->only(['index', 'show', 'edit'])
    ->names('admin.constancias');

Route::resource('validar', AcumuladoController::class)
    ->only(['index', 'show', 'edit','update','destroy'])
    ->names('admin.validars');
