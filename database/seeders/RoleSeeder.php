<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roleAdmin = Role::create(['name'=>'Admin']);
        $roleJefe = Role::create(['name'=>'Jefe']);
        $roleCord = Role::create(['name'=>'Cordinador']);
        $roleVoae = Role::create(['name'=>'VOAE']);
        //permisos para ingresar al Dashboard
        Permission::create(['name'=>'admin.home','description'=>'Acceder al Dashboard'])->syncRoles([$roleAdmin,$roleJefe,$roleCord,$roleVoae]);
        
        //permisos para Usuarios
        Permission::create(['name'=>'admin.users.index','description'=>'Ver lista de usuarios'])->assignRole($roleAdmin);
        //Permission::create(['name'=>'admin.users.update','description'=>'Actualizar usuarios'])->assignRole($roleAdmin);
        Permission::create(['name'=>'admin.users.edit','description'=>'Actualizar usuarios'])->assignRole($roleAdmin);

        //permisos para Centros
        Permission::create(['name'=>'admin.centros.index','description'=>'Ver Centros'])->assignRole($roleAdmin);
        Permission::create(['name'=>'admin.centros.create','description'=>'Crear Centros'])->assignRole($roleAdmin);
        Permission::create(['name'=>'admin.centros.edit','description'=>'Editar Centros'])->assignRole($roleAdmin);
        Permission::create(['name'=>'admin.centros.destroy','description'=>'Eliminar Centros'])->assignRole($roleAdmin);
        //permisos para Facultades
        Permission::create(['name'=>'admin.facultads.index','description'=>'Ver Facultades'])->assignRole($roleAdmin);
        Permission::create(['name'=>'admin.facultads.create','description'=>'Crear Facultades'])->assignRole($roleAdmin);
        Permission::create(['name'=>'admin.facultads.edit','description'=>'Editar Facultades'])->assignRole($roleAdmin);
        Permission::create(['name'=>'admin.facultads.destroy','description'=>'Eliminar Facultades'])->assignRole($roleAdmin);
        //permisos para Carreras
        Permission::create(['name'=>'admin.carreras.index','description'=>'Ver Carreras'])->assignRole($roleAdmin);
        Permission::create(['name'=>'admin.carreras.create','description'=>'Crear Carreras'])->assignRole($roleAdmin);
        Permission::create(['name'=>'admin.carreras.edit','description'=>'Editar Carreras'])->assignRole($roleAdmin);
        Permission::create(['name'=>'admin.carreras.destroy','description'=>'Eliminar Carreras'])->assignRole($roleAdmin);
        //permisos para Carreras
        Permission::create(['name'=>'admin.roles.index','description'=>'Ver Roles'])->assignRole($roleAdmin);
        Permission::create(['name'=>'admin.roles.create','description'=>'Crear Roles'])->assignRole($roleAdmin);
        Permission::create(['name'=>'admin.roles.edit','description'=>'Editar Roles'])->assignRole($roleAdmin);
        Permission::create(['name'=>'admin.roles.destroy','description'=>'Eliminar Roles'])->assignRole($roleAdmin);
        //permisos para Estudiantes
        Permission::create(['name'=>'admin.estudiantes.index','description'=>'Ver Estudiantes'])->syncRoles([$roleAdmin,$roleJefe,$roleCord,$roleVoae]);
        Permission::create(['name'=>'admin.estudiantes.create','description'=>'Crear Estudiante'])->syncRoles([$roleAdmin,$roleJefe,$roleCord,$roleVoae]);
        Permission::create(['name'=>'admin.estudiantes.edit','description'=>'Editar Estudiante'])->syncRoles([$roleAdmin,$roleJefe,$roleCord,$roleVoae]);
        Permission::create(['name'=>'admin.estudiantes.destroy','description'=>'Eliminar Estudiante '])->syncRoles([$roleAdmin,$roleJefe,$roleCord,$roleVoae]);

        //permisos para Actividades
        Permission::create(['name'=>'admin.actividads.index','description'=>'Ver Actividades'])->syncRoles([$roleAdmin,$roleJefe,$roleCord,$roleVoae]);
        Permission::create(['name'=>'admin.actividads.create','description'=>'Crear Actividades'])->syncRoles([$roleAdmin,$roleJefe,$roleCord,$roleVoae]);
        Permission::create(['name'=>'admin.actividads.edit','description'=>'Editar Actividad'])->syncRoles([$roleAdmin,$roleJefe,$roleCord,$roleVoae]);
        Permission::create(['name'=>'admin.actividads.destroy','description'=>'Eliminar Actividad '])->syncRoles([$roleAdmin,$roleJefe,$roleCord,$roleVoae]);

        //permisos para Codigos
        Permission::create(['name'=>'admin.codes.index','description'=>'Ver Actividades para generar Códigos'])->syncRoles([$roleAdmin,$roleJefe,$roleCord,$roleVoae]);
        Permission::create(['name'=>'admin.codes.create','description'=>'Crear Actividades códigos a actividades'])->syncRoles([$roleAdmin,$roleJefe,$roleCord,$roleVoae]);
        Permission::create(['name'=>'admin.codes.show','description'=>'Ver Códigos'])->syncRoles([$roleAdmin,$roleJefe,$roleCord,$roleVoae]);
        Permission::create(['name'=>'admin.codes.update','description'=>'Desactivar Códigos '])->syncRoles([$roleAdmin,$roleJefe,$roleCord,$roleVoae]);
        Permission::create(['name'=>'admin.codes.destroy','description'=>'Eliminar Códigos '])->syncRoles([$roleAdmin,$roleJefe,$roleCord,$roleVoae]);

        //permisos para Constancias
        Permission::create(['name'=>'admin.constancias.index','description'=>'Generar Constancias'])->syncRoles([$roleAdmin,$roleJefe,$roleCord,$roleVoae]);
        Permission::create(['name'=>'admin.constancias.edit','description'=>'Crear Constancias'])->syncRoles([$roleAdmin,$roleJefe,$roleCord,$roleVoae]);
        Permission::create(['name'=>'admin.constancias.show','description'=>'Descargar Constancias'])->syncRoles([$roleAdmin,$roleJefe,$roleCord,$roleVoae]);

        //permisos para validaciones
        Permission::create(['name'=>'admin.validars.index','description'=>'Ver lista de validaciones'])->syncRoles([$roleAdmin,$roleJefe,$roleCord,$roleVoae]);
        Permission::create(['name'=>'admin.validars.create','description'=>'Ver validación'])->syncRoles([$roleAdmin,$roleJefe,$roleCord,$roleVoae]);
        Permission::create(['name'=>'admin.validars.edit','description'=>'Acreditar validación '])->syncRoles([$roleAdmin,$roleJefe,$roleCord,$roleVoae]);
        Permission::create(['name'=>'admin.validars.destroy','description'=>'Desestimar validación '])->syncRoles([$roleAdmin,$roleJefe,$roleCord,$roleVoae]);



        


    }
}
