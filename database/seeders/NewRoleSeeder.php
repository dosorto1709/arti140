<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;


class NewRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
            $roleJefe = Role::create(['name'=>'Jefe']);
        $roleCord = Role::create(['name'=>'Cordinador']);
        $roleVoae = Role::create(['name'=>'VOAE']);
         */

        // buscar el role llamado "Admin"
        $adminRole = Role::where('name', 'Admin')->first();
        $rolJefe = Role::where('name', 'Jefe')->first();
        $rolCord = Role::where('name', 'Cordinador')->first();
        $rolVoae = Role::where('name', 'VOAE')->first();

        Permission::create(['name' => 'admin.validars.show', 'description' => 'Ver Validar'])
            ->syncRoles([$adminRole, $rolJefe, $rolCord, $rolVoae]);
    }
}
