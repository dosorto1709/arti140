<?php

namespace Database\Seeders;

use App\Models\Centro;
use Illuminate\Database\Seeder;

class CentroSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Centro::create([
            'nombre'=>"CURLP",
            'ciudad'=>'Choluteca',
            'depto'=>'Choluteca',
            'logo'=>'public/images/curlp.png',
            'voae'=>'Lic. Eimy López',
            'direccion'=>'Salida a San Marcos de Colón km 5',
            'url'=>'www.curlp.unah.edu.hn',
        ]);
    }
}
