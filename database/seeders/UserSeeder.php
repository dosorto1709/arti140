<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use \App\Models\User;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'=>"Admin",
            'email'=>'admin@art140.unah.edu.hn',
            'password'=>bcrypt('P@sswd123'),
            'centro_id'=>1,
            'facultad_id'=>1,
            'carrera_id'=>1,
        ])->assignRole('Admin');
    }
}
