<?php

namespace Database\Seeders;

use App\Models\Carrera;
use Illuminate\Database\Seeder;

class CarreraSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Carrera::create([
            'nombre'=>"Ingeniería en Sistemas",
            'telefono'=>'2222-2222',
            'ext'=>'3232',
            'cordinador'=>'Ing. Dorian Ordoñez',
            'facultad_id'=>1,
            'centro_id'=>1,
            'logo'=>'public/images/is.png'
        ]);
        
    }
}
