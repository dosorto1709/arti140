<?php

namespace Database\Seeders;

use App\Models\Facultad;
use Illuminate\Database\Seeder;

class FacultadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Facultad::create([
            'nombre'=>"Facultad de Ingeniería",
            'centro_id'=>1,
            'logo'=>'public/images/facing.png',
        ]);
        Facultad::create([
            'nombre'=>"Facultad de Ciencias",
            'centro_id'=>1,
            'logo'=>'public/images/facing.png',
        ]);
    }
}
