<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCarrerasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carreras', function (Blueprint $table) {
            $table->id();
            $table->string("nombre");
            $table->string("telefono");
            $table->string("ext");
            $table->string("cordinador");
            $table->string("logo");
            $table->unsignedBigInteger("facultad_id");
            $table->foreign("facultad_id")->references("id")->on("facultads");
            $table->unsignedBigInteger("centro_id");
            $table->foreign("centro_id")->references("id")->on("centros");
            //$table->unsignedBigInteger("centro_id");
            //$table->foreign("centro_id")->references("id")->on("centros")->onDelete("cascade");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carreras');
    }
}
