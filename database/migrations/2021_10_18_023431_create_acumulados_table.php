<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAcumuladosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acumulados', function (Blueprint $table) {
            $table->id();
            $table->enum('estado', ['Pendiente', 'Acreditado','No Asignado'])->default('Pendiente');
            $table->unsignedBigInteger("validado_user_id")->nullable() ;
            $table->unsignedBigInteger("code_id");
            $table->foreign("code_id")->references("id")->on("codes");
            $table->unsignedBigInteger("estudiante_id");
            $table->foreign("estudiante_id")->references("id")->on("estudiantes");
            $table->unsignedBigInteger("actividad_id");
            $table->foreign("actividad_id")->references("id")->on("actividads");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acumulados');
    }
}
