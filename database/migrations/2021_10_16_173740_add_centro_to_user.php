<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCentroToUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->unsignedBigInteger("centro_id")->nullable();
            $table->foreign("centro_id")->references("id")->on("centros");
            $table->unsignedBigInteger("facultad_id")->nullable();
            $table->foreign("facultad_id")->references("id")->on("facultads");
            $table->unsignedBigInteger("carrera_id")->nullable();
            $table->foreign("carrera_id")->references("id")->on("carreras");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('centro_id');
            $table->dropColumn('facultad_id');
            $table->dropColumn('carrera_id');
        });
    }
}
