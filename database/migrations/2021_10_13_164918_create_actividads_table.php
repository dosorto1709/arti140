<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActividadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actividads', function (Blueprint $table) {
            $table->id();
            $table->string("nombre");
            $table->integer("horas");
            $table->date('fecha');
            $table->enum('dimension', ['Social', 'Cultural-Artístico','Deportiva','Científico-Académico']);
            $table->unsignedBigInteger("centro_id");
            $table->foreign("centro_id")->references("id")->on("centros");
            $table->unsignedBigInteger("facultad_id");
            $table->foreign("facultad_id")->references("id")->on("facultads");
            $table->unsignedBigInteger("carrera_id");
            $table->foreign("carrera_id")->references("id")->on("carreras");
            $table->boolean('is_voae')->default(false);
            $table->boolean('aprobada')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('actividads');
    }
}
